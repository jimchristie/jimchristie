---
layout: post
title: Is Scrum Broken?
snippet: >-
  I've read a number of articles recently arguing that Scrum doesn't work. With
  so many people having similar negative experiences, I'm starting to wonder: Is
  there a valid point here? 
category: Work
featured: true
enabled: true
featuredImage:
  fill: contain
  image: /img/uploads/broken-bulb-still-working.jpg
  orientation: landscape
ogDescription: >-
  I've read a number of articles recently arguing that Scrum doesn't work. With
  so many people having similar negative experiences, I'm starting to wonder: Is
  there a valid point here?
ogImage: /img/uploads/broken-bulb-still-working.jpg
date: '2021-02-26 03:28:30'
---
I read an article a little while back called "[Why Scrum is killing your product](https://uxdesign.cc/why-scrum-is-killing-your-product-febfcab464e5)." The basic gist of the article is that Scrum prioritizes output over outcomes, largely through a Product Owner role that is focused on the backlog more than customers. I could talk at length at how that's not how it's supposed to work. That is, in fact, covered by a lot of comments on the article. However, as I've thought about this more and more, I've started to wonder: Is there a valid point here? 

### Lots of people have the same experience

The thing that has me wondering if the author has a point is that he [isn't unique](https://www.google.com/search?q=bad+scrum&rlz=1CAPVCB_enUS918&oq=bad+scrum&aqs=chrome.0.69i59j69i57j69i60l4j0i271l2.1396j0j4&sourceid=chrome&ie=UTF-8) in his experience with what the agile community likes to call "bad scrum." I see posts similar to this one on a regular basis. They pop up in my LinkedIn feed and my Google search results. They're linked to from other articles. I even see a ton of articles and videos where people are responding to these negative impressions of Scrum, usually to the tune of "[you're doing it wrong.](https://www.google.com/search?rlz=1C1GCEA_enUS883US884&sxsrf=ALeKk02dufn3yo0nmIO3h-29noWT7pgqdQ%3A1614346965802&ei=1fo4YOyxMNKxtQbChoqICA&q=doing+scrum+wrong&oq=doing+scrum+wrong&gs_lcp=Cgdnd3Mtd2l6EAMyBggAEBYQHjIGCAAQFhAeOgcIIxCwAxAnOgcIABCwAxBDOgQIIxAnOgQIABBDOgsILhCxAxDHARCjAjoICC4QxwEQowI6BQgAELEDOggIABCxAxCDAToFCC4QsQM6BQgAEJECOggILhDHARCvAToCCAA6AgguOgsIABCxAxCDARDJAzoICAAQFhAKEB46BQgAEIYDUJvuCljcgwtg84ULaAJwAngAgAGiAogBoRSSAQYwLjE0LjOYAQCgAQGqAQdnd3Mtd2l6yAEKwAEB&sclient=gws-wiz&ved=0ahUKEwis8MKL14fvAhXSWM0KHUKDAoEQ4dUDCA0&uact=5)" 

In a previous post, I wrote about [three reasons that people push back against processes](/blog/just-follow-the-process/). While Scrum is a framework for managing a process, I think the same reasoning applies. In fact, all of the articles that I've mentioned on either side of the argument boil down to one of two of those reasons. All of the Scrum naysayers are saying that Scrum is fundamentally broken. All of the Scrum cheerleaders are arguing that the naysayers don't understand Scrum. (The third reason that people don't follow processes - that it takes up too much time and effort - is sometimes brought up about Scrum, but rarely enough that I don't want to focus on it.)

In that previous post, I also talked about how there's a point when enough people don't understand your ~~process~~ framework that it's time to consider that your ~~process~~ framework might actually be broken. 

[Oh, no](https://giphy.com/gifs/facepalm-xUOwGaTTW1jV79lUdO). [No](https://giphy.com/gifs/schittscreek-comedy-pop-tv-26hkhKd2Cp5WMWU1O). [No](https://giphy.com/gifs/the-office-no-michael-scott-ToMjGpx9F5ktZw8qPUQ). [No](https://giphy.com/gifs/luke-skywalker-Xjo8pbrphfVuw)! This is _very_ uncomfortable territory for me. I _like_ Scrum. I'm one of the aforementioned Scrum cheerleaders! I don't want to be bashing something that I'm so [professionally invested in](/work/#certifications). Ok, Jim. Deep breaths. One of the hallmarks of Scrum, and agility is to learn through inspecting and adapting. So, let's inspect. 

The Scrum cheerleaders generally have a point that the naysayers are doing it wrong. The naysayers generally are actually doing it wrong. But how could so many people getting it so wrong? Indeed, in my own personal experience, I've seen more people do Scrum poorly than those who did it well. But how could so many people be getting it so wrong?

### Scrum is old

Scrum was first mentioned in 1986 and [introduced as a formalized as a framework in 1995](http://www.jeffsutherland.org/oopsla/schwapub.pdf). That first iteration of Scrum is very recognizable as the Scrum that is so widely used today. Certainly, there have been refinements. It was mostly focused on the processes. The Scrum Master role didn't exist. The Product Owner role was called a Product Manager and had less formalized responsibilities. But the sprint was there. The Development Team was there. There were Planning and Closure phases, precursors to our modern Sprint Planning and subsequent Retrospective and Review sessions. The foundations were all there. 

Just for the sake of context, let's take a step back from Scrum and talk about some other things that were going on in the software industry at about that same time. 

* 1985 - The first version of Windows is released.
* 1989 - The general public has access to the internet for the first time.
* 1990 - The first web browser is released. 
* 1991 - The first version of Linux is released.  
* 1994 - Online banking is available for the first time.

All of those things were happening while Scrum was in its gestation. Since then, we've seen a complete revolution in the nature and use of software. Online banking is ubiquitous. Windows has changed so much that it's unrecognizable from its [first version](https://en.wikipedia.org/wiki/Windows_1.0#/media/File:Windows1.0.png). Linux has gone from a computer science student's pet project that could only be run on one specific type of computer powering the majority of the servers in the world. And, omg, the internet. The internet has completely revolutionized the way the world works. Not only are we all connected in ways that were barely dreamed of at the time, but entire applications that required CD installation are now quaint compared to what's available in the browser. Even the browser itself has fundamentally changed to the point where it can be run as an operating system with minimal changes. 

Yet, Scrum is fundamentally the same. 

### Scrum doesn't scale well

This explosion in technology has required organizations to scale up _massively_. I couldn't find data going back as far as the infancy of Scrum, but [in just the last four years software developer employment has grown 29%.](https://slashdata-website-cms.s3.amazonaws.com/sample_reports/EiWEyM5bfZe1Kug_.pdf) Extend that back a couple of decades and I'm guessing that the growth has been orders of magnitude. That's just the developers. Add in people doing product management work, requirements gathering, interface design, UX research, and all of the other stuff that goes into developing software and you're talking about insane growth across the industry. 

Of course, there are multiple scaling frameworks that organizations attempt to layer on top of Scrum. Maybe that's the right approach, but we also shouldn't discount the idea that maybe Scrum itself should be adapted to larger organizations.  

### Scrum doesn't address the necessary culture shift

A good Scrum implementation requires nearly everyone in an organization to change their ways of working in some way or another. Yet, the standard transformation practice is to bring in an outside agency, train the new Scrum teams, and announce that "we're agile now." The necessary culture shift across an organization implementing Scrum teams barely gets mentioned. 

As a result, the rest of the org remains the same. Scrum teams are expected to conform what they've learned to the existing organizational practices. The teams are often aligned around sub-systems rather than products. Product Owners often don't actually own their products, or even their sub-systems. Execs still expect rigid timelines. Bonuses are still tied to measures of output rather than outcomes. Stakeholders don't have the time to work with teams. Teams don't have access to end users. 

It's no wonder that so many people have a bad experience. 

### So what do we do?

The first step in solving any problem is to simply recognize that there's a problem. It seems pretty clear to me that we agilists have not one problem, but _problems_ to solve with Scrum. I honestly don't know how to solve them, but I do think they can be solved. 

What's more, our organizations _need_ to get these problems solved. They bring in Agile Coaches to help solve these problems. So far, that seems to be the _de facto_ solution. Coaching certainly isn't a bad thing, but things in the software industry change too quickly for a company to dither around with a transformation that takes years. Scrum is due for something more concrete, more broadly and immediately applicable. It needs to catch up to the industry that it helped revolutionize. 

### Final note

I really want to stress here that despite how critical of Scrum this article has been, I _like_ Scrum. I _believe_ in Scrum. However, I can't ignore the fact that Scrum simply isn't working for a significant number of people. Dogmatically telling all of those people that they're doing it wrong isn't going to help the evolution of Scrum. Embracing their struggles and looking for solutions is the only way to move forward from where we are.

> <span class="small-font">Updated 7/30/21 to reflect a better understanding of "methodology" vs. "framework."</span>