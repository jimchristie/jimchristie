---
layout: post
title: Dev Teams Need Sportsball Coaches
snippet: >-
  Any time that we look at professional sports teams, we see one thing in
  common: they all have coaches. Just like a sports team needs a coach to reach
  the championships, a development team needs a coach to reach its full
  potential.
category: Work
featured: true
enabled: true
featuredImage:
  fill: contain
  image: /img/uploads/sportsball-play.jpg
  orientation: landscape
date: '2019-04-18 08:06:04'
---
> Update: I use the term "sportsball" in this post. After publishing, I learned that this term is sometimes considered to be derogatory towards sports fans. That was not my intention. My aim was to call attention to and make light of lack of sports knowledge. Please forgive my ignorance. 

I'm not really much of a sportsball fan. The only sport I really follow is cycling, in spite of [all its flaws](https://en.wikipedia.org/wiki/List_of_doping_cases_in_cycling). I also tend to watch the Olympics when they come around. That's about it. 

Even in spite of that, I like to talk and think about coaching software development teams the same way we talk and think about coaching sportsball teams. That's because even if you're not a sportsball fan, as I am not, you probably have a decent idea of what a sportsball coach does. 

Another good reason to look to sportsball is for examples of good coaching practices is that it's a field (no pun intended) that has had a long time to develop a great approach to coaching. 

### All championship teams have coaches

It doesn't matter if you're watching baseball, football, fútbol, rugby, or the Tour de France, every professional sports team has one thing in common: they all have coaches. Sometimes they have more than one coach. 

We take it for granted that sports teams need coaches to win. Why don't we do the same for other aspects of our lives? A coach is a teacher, a mentor, and an adviser. A coach can make observations from the sideline that you can't see when you're in the middle of the field. A coach pushes individuals to be better they ever thought possible. A coach is what makes the team more than the sum of those individuals. Their input is so invaluable that most people would laugh if you suggested a sports team could do well without one. 

Yet, for some reason, a dev team asking for a coach often feels like they've failed somehow. This couldn't be further from the truth. A coach is just as valuable to a software team as they are to a sportsball team. A coach will push a software development team to be the best team that they can possibly be. 

This is precisely why Scrum and XP both advocate for a coach on every team. XP directly calls this role a coach, while Scrum calls the role a Scrum Master, but it amounts to the same thing. It's a person who monitors the process, watches for possible improvements, and helps the team to work together. 

### Coaches don't play the game

No matter what sport you watch, coaches never play the game. In fact, the only time they ever step on the field/court/track is when the game is stopped. They might pop out onto the field during a time out or something, but the referee will penalize the team if the coach is on the field while the game is in play.  Coaches need that objectivity in order to effectively coach. 

If you compare software development to a game, the product is the "ball," releasing your software is the "goal," and the "game" consists of developing requirements, building designs, writing code, running tests, deploying, measuring impact, and handoffs between the various stages. Essentially, the game is the whole software life cycle plus the business input that goes into the product. 

Software development coaches can't be a part of the game and still be effective coaches. Software development coaches watch the process, make observations and suggestions, ask questions, facilitate discussions, and make sure that everyone else is playing the "game" as best as they can. This is incredibly difficult to do if you're on the field, playing the game.

### The best coaches help players to play without them

If we look at a pee-wee team, the coaches are vastly different than the coaches at the professional level. In a pee-wee team, the coaches are directive and focus on individuals. "Stand here." "Hold the bat like this." As we move to intermediate levels, coaches tend to focus on developing the team dynamics a bit more. They focus on how the team works together, but still keep a close watch on individual growth and development. 

Finally, at the elite level, coaches are almost solely watching the game wholistically. The coach doesn't have to tell players what to do because they all know how the team works together. The coach mostly pays attention to how well the players are working together. They're making observations about tiny little things to get incremental gains. At the elite level, these incremental gains are the difference between the regular season and the finals, between the finals and the championship. 

### Wrapping up

Lots of amateur teams don't have coaches. Your weekend sportsball team might not. But you don't want your development team to be a weekend amateur team. You want them to be a championship level professional team. They fastest way to get there, and maybe the only way, is through a coach.
