---
layout: post
title: The Biggest Loser Between Musk and Zuck
series: ''
snippet: >-
  Elon Musk and Mark Zuckerberg have been duking it out lately. The most visible
  parts of their clash has played out across their respective social media
  platforms, often in [juvenile
  fashion](https://twitter.com/elonmusk/status/1678222776908275712). While their
  battle hasn't actually spilled over into physical combat
  [yet](https://www.forbes.com/sites/antoniopequenoiv/2023/07/12/zuckerberg-vs-musk-everything-we-know-about-the-possible-cage-fight/?sh=322e7fdb2cc8),
  it's also playing out in the real world beyond social media space.
category: Life
tags:
  - tag: ''
featured: false
enabled: true
ogDescription: >-
  Elon Musk and Mark Zuckerberg have been duking it out lately. The most visible
  parts of their clash has played out across their respective social media
  platforms, often in [juvenile
  fashion](https://twitter.com/elonmusk/status/1678222776908275712). While their
  battle hasn't actually spilled over into physical combat
  [yet](https://www.forbes.com/sites/antoniopequenoiv/2023/07/12/zuckerberg-vs-musk-everything-we-know-about-the-possible-cage-fight/?sh=322e7fdb2cc8),
  it's also playing out in the real world beyond social media space.
date: '2023-07-14 08:31:32'
---
Elon Musk and Mark Zuckerberg have been duking it out lately. The most visible parts of their clash has played out across their respective social media platforms, often in [juvenile fashion](https://twitter.com/elonmusk/status/1678222776908275712). While their battle hasn't actually spilled over into physical combat [yet](https://www.forbes.com/sites/antoniopequenoiv/2023/07/12/zuckerberg-vs-musk-everything-we-know-about-the-possible-cage-fight/?sh=322e7fdb2cc8), it's also playing out in the real world beyond social media space. 

While Musk has been busy [burning Twitter to the ground](https://www.bloomberg.com/news/newsletters/2023-02-03/elon-musk-s-first-100-days-at-twitter-a-flaming-dumpster) in the name of "[free speech](https://www.thedailybeast.com/elon-musk-worst-free-speech-hero-ever)," Zuckerberg has launched a new app, [Threads](https://creators.instagram.com/blog/introducing-threads), which has been dubbed a "Twitter [killer](https://www.nytimes.com/2023/07/05/technology/threads-app-meta-twitter-killer.html)." Musk has since [threatened a lawsuit](https://twitter.com/TitterDaily/status/1677025032554545153?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1677042708756439041%7Ctwgr%5E94b7694194b6e5642045c401c0197a9aae80a60f%7Ctwcon%5Es2_&ref_url=https%3A%2F%2Fwww.reuters.com%2Ftechnology%2Ftwitter-is-threatening-sue-meta-over-threads-semafor-2023-07-06%2F), alleging that Zuck's company stole a bunch of Twitter employees in order to gain and use Twitter's intellectual property to build Threads. So it seems that their battle may spill over into the courtroom as well. 

While it's hard to predict who will win when two titans of industry clash, one thing that I can safely say is who the biggest loser is: us.

In the end, it doesn't really matter if Thread eclipses Twitter or goes the way of [Google+](https://en.wikipedia.org/wiki/Google+). You and I, the little people, will end up being the biggest losers. 

The problem isn't Musk or Zuckerberg themselves, it's the way that more and more of our online presence is being consolidated into a few giant for-profit corporations. Facebook, Amazon, Google, and Microsoft have become involved in almost every aspect of our online lives. Not only are they visible through sites and ads, but their platforms and technologies collectively form the backbone of the internet. Microsoft and Amazon collectively make up about 75% of the server infrastructure of the entire internet.
