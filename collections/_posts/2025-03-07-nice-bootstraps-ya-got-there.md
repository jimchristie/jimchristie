---
layout: post
title: Nice Bootstraps Ya Got There
snippet: |-
  I taught myself to program. I'm fairly proud of this fact.

  There's just one problem. It's not a fact. It's fiction.
category: Life
featured: true
enabled: true
featuredImage:
  image: /img/uploads/bootstrap.jpg
  orientation: portrait
ogDescription: |-
  I taught myself to program. I'm fairly proud of this fact.

  There's just one problem. It's not a fact. It's fiction.
date: '2025-03-07 08:58:52'
---
I taught myself to program. I'm fairly proud of this fact.

I'm not some whiz kid who learned to program in his parents' basement at age fifteen. Quite a long way from it, actually. I was in my thirties when I wrote my first line of code. I had spent my whole adult life moving from one low paying job to another. I was staring down the prospect of living the rest of my life paycheck to paycheck in a one bedroom apartment in a bad part of town. I didn't like that prospect, so I did something different. 

It wasn't the first time. About a decade earlier, I was in a similar position. At that time, I hadn't gone to school. I had been rejected by the military. I was working as a cook. I hated it. The pay sucked. The work was hot, sweaty, and stressful. I was a stinky, greasy mess at the end of every shift. Something had to change, so I went to college.

I wasn't exactly what you'd call a "dedicated student." I barely maintained a full-time status. I failed and re-took a few classes. I majored in art because I didn't really know what else to do and it sounded fun. I switched to philosophy because I liked those classes better. None of this was going to get me rich, but I did manage to land a student job at the university's library. At least I was out of the service industry. 

A decade after landing that first library job, I had moved to a larger town with a larger university, but nothing much had really changed. I hadn't finished college. I wasn't even enrolled anymore. I was on my third library job, now at the public library. I was once again stuck. 

So I grabbed a book on web programming and started to learn. I took online courses too, some free and some available to the public through the library. 

Learning to program changed my life. I finished school so I could get a job in the private sector. There were opportunities for promotions, opportunities for continuing education, opportunities to be mentored and eventually do some mentoring of my own.  

And, of course, I was making more money. I could afford to travel. I bought a house. I started saving. 

All of that because I taught myself to program. And again, I am really proud of that. 

There's just one problem: it's fiction. 

Authors taught me to program through their books. Online teachers taught me to program through video lectures. Teams of software developers and a variety of other supporting roles taught me to program through self-directed. Once I had learned enough from those sources to get a job in programming, my coworkers taught me to program. 

On top of all of that, those resources were only available because of the library. My supposed self-education had been funded by tax dollars. I hadn't paid a dime for it. 

We talk about "pulling yourself up by your bootstraps." That, like my self-taught programming, is a nice fiction. Setting aside the fact that [the expression was initially meant to represent something that's impossible](https://uselessetymology.com/2019/11/07/the-origins-of-the-phrase-pull-yourself-up-by-your-bootstraps/), there's the question of where the bootstraps came from. Those metaphorical bootstraps, and the boots that they're attached to, were manufactured in a factory, made from the leather of a cow that was butchered in a slaughterhouse, after being transported from a ranch, that was managed by dozens or hundreds of people. 

None of us exists in isolation. You might put in some work to better yourself in one way or another, and that's admirable, but we can't ignore the fact that we're always reliant on society at large. We owe our successes to that society and of the people who contributed to our successes as much, and likely more, than we do to our own efforts.
