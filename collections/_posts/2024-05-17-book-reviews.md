---
layout: post
title: Book Reviews!
snippet: >-
  I love to read. It occurred to me that there are multiple reasons that I
  should be writing about what I read. I've built out some content types and
  published my first book review. I hope you find it helpful.
category: Life
featured: true
enabled: true
featuredImage:
  focalPoint:
    leftRight: ''
  image: /img/uploads/book-with-glasses.jpg
  orientation: landscape
date: '2024-05-17 12:46:22'
---
When I was a kid, I got into trouble pretty regularly and my mom would ground me. No TV, no music, no hanging out with friends, just sit and be bored. The one thing that she never took away from me is reading. Luckily for me, I liked reading. I found out much later, from my dad, that it irritated the heck out of her that I was supposed to be grounded and was sitting there doing something I enjoyed. But still, it's not like she could tell me not to read. That'd be ridiculous. 

My love of reading survives to this day. I read fiction in bed at night. My wife and I read books together. I read non-fiction for work, personal growth, and just to find out about topics that I'm curious about. These days, I typically have three or so books going at one time. 

It occurred to me at some point that I should write about the stuff I read. It has multiple benefits.

1. It'll give me something to write about, which, is a struggle as [I've mentioned before](/blog/resolution-updates-expansion/).
2. It helps me to process what I've read, which helps to understand the material better.
3. You might find it interesting and/or useful.

I started building out a Book Review content type on my site a while back so that I could include those writings. I even started writing some reviews alongside the web building part of it. It all stagnated. But with my newfound commitment to write more, I finished up the web building and polished up a post. I'm excited to share my first Book Review with you: [How to Read a Book, by Mortimer J. Adler and Charles Van Doren](/blog/how-to-read-a-book/). 

It's really an excellent book, but I'll leave all the fanfare to the Book Review itself. Head on over to my [blog page](/blog/) and check it out!

More to come!
