---
layout: post
title: The Willows are Growing!
snippet: >-
  I just wanted to share a quick update on my [willow
  propagation](/blog/willow-propagation/) project. 

  They're growing like crazy!
featured: true
enabled: true
featuredImage:
  fill: contain
  image: /img/uploads/willow-growth.jpg
  orientation: portrait
date: '2024-05-29 03:52:29'
category: Life
---
I just wanted to share a quick update on my [willow propagation](/blog/willow-propagation/) project. 

They're growing like crazy!

You can see all kinds of little pink growths on the bottom. Those are new roots. 

![close up of root growth](/img/uploads/willow-roots.jpg)

And there are little leaves budding on almost all of them as well!

![willow leaf growth](/img/uploads/willow-leaves.jpg)

There are a few that are growing less than others, but I think they'll come along. There are a few with no leaf growth, but I'm optimistic. I haven't pulled them out and separated them to check that they all have root growth. I don't want to disturb those delicate little growths. But that's what gives me hope that the ones without leaves are just late bloomers. 

These were all from a weeping willow that we have on our property. We have another willow with more upright leaf growth. I trimmed a few cuttings off of it and tried to get them going as well. It's early days yet, but we'll see. I'll probably propagate a ton more, if not this year then next. As I mentioned, I'm hoping to build a living fence. I'll need _a ton_ of willows for that.
