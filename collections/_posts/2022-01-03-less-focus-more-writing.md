---
layout: post
title: 'Less Focus, More Writing'
snippet: >-
  I've been trying (unsuccessfully) for a few years to blog more prolifically.
  I've set targets and goals for myself, only to miss them again and again.
  Clearly, I either need to re-evaluate my goal, or change my approach.
category: Life
featured: true
enabled: true
featuredImage:
  image: /img/uploads/markus-winkler-ecgyrygygee-unsplash.jpg
  orientation: landscape
ogDescription: >-
  I've been trying (unsuccessfully) for a few years to blog more prolifically.
  I've set targets and goals for myself, only to miss them again and again.
  Clearly, I either need to re-evaluate my goal, or change my approach.
date: '2022-01-03 09:56:23'
---
I've been trying (unsuccessfully) for a few years to blog more prolifically. I've set targets and goals for myself, only to miss them again and again. Clearly, I either need to re-evaluate my goal, or change my approach.

### The Goal

More than anything, I just want to keep my writing skills up to snuff. I've had a secondary goal recently of making my blog a more professional representation of myself.  

### The Approach

The approach I've been taking is to write about work things: agile software development, workplace culture, etc. Unfortunately, I think that this approach is serving my secondary goal fairly well but getting in the way of my primary goal. 

### The Change

That being the case, I'm going to go back to what my blog used to be: just a place to talk about whatever the heck I feel like. That'll probably mean work stuff will continue, but also bikes, stuff with my house, woodworking, and any other tech stuff that strikes my interest. 

Hopefully, this'll help me to just write more. At this point, I'm trying to manage my expectations of myself, so we'll see what happens.  
