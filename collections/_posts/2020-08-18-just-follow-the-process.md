---
layout: post
title: '"Just follow the process"'
snippet: >-
  Variations on this are something I hear fairly frequently, from lots of people
  in lots of organizations. So why does this happen? Why don't people follow the
  process. In my experience, there are three reasons.
category: Work
featured: false
enabled: true
date: '2020-08-25 09:44:13'
---
Variations on this are something I hear fairly frequently, from lots of people in lots of organizations.

> If they'd just follow the process...<br>
> We have a process, why can't they follow it?<br>
> We have the process so that stuff like this doesn't happen.

So why does this happen? Why don't people follow the process. In my experience, there are three reasons:

1. Someone doesn't know or understand the process.
2. Someone believes that the process takes up too much time and effort while delivering too little value.
3. The process is broken.

The first is sometimes the easiest to remedy. If someone fails to follow a process, it's easy enough to teach them. It can be as simple as that. 

The second can also be an education issue. Sometimes, a process exists to provide benefit that isn't immediately apparent to those that are going through it. If that's the case, it can be just as easy to remedy if the problem is a lack of visibility. 

The third is, of course, the thorniest problem. It's also sometimes the most difficult to recognize. One hint can be that multiple people don't understand the process. At that point, you have to ask yourself if it's truly an education problem, or if the process is simply too difficult to understand. Another way that the process could be broken is that the person who doesn't see the value is correct: the value that the process provides really isn't justified by the take too much time and effort it takes. In either case, it could be time to streamline your process, or even redesign it entirely.

Next time you find yourself wondering why a process broke down, stop to ask yourself whether it's the people or the process that's failing you.
