---
layout: post
title: The Efficiencies of Firefighters and Doctors
snippet: >-
  We all want to be efficient. We want to get stuff done. We want to make the
  most of our time. _That's_ efficiency. Unfortunately, those two things -
  getting stuff done quickly and making the most of our time - are often at
  odds.
category: Work
featured: true
enabled: true
featuredImage:
  fill: cover
  focalPoint:
    leftRight: left
  image: /img/uploads/do-more-screensaver.jpg
  orientation: landscape
ogDescription: >-
  We all want to be efficient. We want to get stuff done. We want to make the
  most of our time. _That's_ efficiency. Unfortunately, those two things -
  getting stuff done quickly and making the most of our time - are often at
  odds.
ogImage: /img/uploads/do-more-screensaver.jpg
date: '2021-02-12 08:35:13'
---
We all want to be efficient. We want to get stuff done quickly. We want to make the most of our time. _That's_ efficiency. Unfortunately, those two things - getting stuff done quickly and making the most of our time - are often at odds. If we take a look at doctors and firefighters, we can see that there are two types of efficiency and how they don't always play nice together. 

![doctor with stethoscope](/img/uploads/doctor-with-stethoscope.jpg)

### Seeing a Doctor

When you decide you need to make a doctor's appointment, there's a typical path that you follow. 

1. Find the contact info.
2. Make the appointment.
3. Wait a few days, weeks, perhaps a month or so.
4. Drive to the doctor's office. 
5. Sign in at the front desk.
6. Wait in the waiting room.
7. Go to the exam room.
8. Wait some more.
9. See a nurse. 
10. Wait some more.
11. See the doctor.

Your goal, and the thing that gave you value, was to see the doctor. All those other steps don't provide value _per se_, they're means to an end. (Seeing the nurse is an arguable exception, as he or she is providing some of the functions that the doctor would otherwise perform.) In a weeks-long process, you receive roughly five to ten minutes of value.

You might look at the above list and think, "But all of those steps are valuable because they move you toward the end goal." However, imagine if money was no obstacle and you wanted to see a doctor without all the fuss. You might have a personal physician on call. You'd call him or her directly and get a consultation over the phone. That's near-instant value! That may be enough, or the doctor might say that he or she wants to see you. A short time later, the doctor would arrive wherever you are. All those steps that you'd shortcut if you just had the money, those don't actually provide value. 

![fire fighter with pole next to fire truck](/img/uploads/firefighter.jpg)

### Services from the Fire Department

When you realize you need services from the fire department, you typically follow a much different path.

1. Call an easy-to-remember emergency services number ([911, 999, 000, etc.](https://www.countryliving.com/uk/wildlife/countryside/news/a1553/emergency-numbers-in-countries-abroad/)).
2. Talk to the dispatcher. 
3. Wait [5-10 minutes](https://www.lexipol.com/resources/blog/understanding-and-measuring-fire-department-response-times/).
4. The fire department addresses the emergency ([firefighters do a lot more than just putting out fires](https://en.wikipedia.org/wiki/Fire_department#Responsibilities)).

In this case, taking care of the emergency is the thing that gave you value. And unlike the doctor, you spend a very small amount of time waiting or engaged in supporting activities in comparison to the time that you receive value from the servicer.

### Two Types of Efficiency

At this point, you might be saying to yourself "Doctors are horribly inefficient!" However, don't jump to that conclusion just yet. Doctors are actually incredibly efficient. It's just a different type of efficiency than firefighters. 

#### Resource Efficiency

Doctors are _resource efficient_. Resource efficiency is a measure of how much time or effort is wasted when the resources in a system are not providing value, or providing value sub-optimally. In the medical industry, doctors are a resource. In fact, they're an _expensive_ resource. Because of this, we make sure that doctors are using their time to the greatest extent. They're busy with patients all day long. If they weren't, they'd be even more expensive. 

What's more, they delegate tasks that don't directly deliver value to patients to support staff. There are people who schedule appointments, order supplies, clean the office, and all of those other tasks that need to be done in order to enable the doctor to do his or her job. 

#### Flow Efficiency

Firefighters, on the other hand, are _flow efficient._ Flow efficiency is a measure of how quickly we realize value in achieving a task or, to put it another way, how quickly that task moves through the system. Much like the doctors in the medical industry, firefighters are the resource in this system. Resolving emergencies is the flow. Because of the high importance of accomplishing their tasks, we build some slack into firefighters' time. This slack is resource inefficiency, but is precisely the thing that allows them to be flow efficient. 

Unlike doctors, firefighters fill their slack time with things not directly related to fighting fires: station upkeep, meal prep for the team, gear maintenance, washing the truck, etc. While all of those are things that need to be done, they're the type of things that could be farmed out to someone else if we were really concerned about keeping firefighters doing the job that only they can do: fighting fires. They're also all things that the firefighters can walk away from at the drop of a hat if a call comes in. 

### Resource vs. Flow

Unfortunately, resource and flow efficiency are usually at odds with one another. In order to maximize resource efficiency, we put the flow items in a queue. This makes sure that the resource is working non-stop. But that queue, by nature, slows down the flow. 

Imagine for a moment that we wanted to get our doctor's visits done more quickly. If we build slack into the doctor's time, we'd move patients through the system more quickly, but the cost of visiting the doctor would go up in order to compensate for that lost time. Doctors are expensive enough that we're ok with waiting a bit to see one in order to keep those costs down.  

Conversely, if we pack firefighters' schedules, they're not going to be as responsive when a call comes in. Can you imagine calling 911 because your house is on fire and the operator telling you that you'll need to wait a couple of hours while the firefighters put out a different fire? I'm guessing that most of us are willing to [cut firefighters some slack in](https://giphy.com/gifs/reactiongifs-SUeUCn53naadO) order to avoid that.

![balanced coffee cups](/img/uploads/balanced-coffee-cups.jpg)

### Balancing the Two

You might be thinking at this point, "Why not try to get to a point where the work arrives at the resource at precisely the right time?" This is certainly the goal of balancing the two types of efficiency. 

Unfortunately, perfect balance is impossible in any system due to natural variation and entropy. Firefighters never know how many calls will come in on any given day, or how long a call will take. Doctors' patients take longer than expected sometimes. Once the system falls behind a little bit, it tends to fall behind further as more delays compound the problem. The only way to catch back up is to build in slack.

We balance the two by doing exactly what the medical community and firefighting communities have done. We build both types of efficiency where we can, but ultimately we determine which type of efficiency is more important for our context and prioritize that over the other. Firefighters increase their resource efficiency by doing those previously mentioned support tasks when they're not fighting fires. Doctors increase their flow efficiency by streamlining their offices. 

It's also worth noting that I've chosen to talk about doctors and firefighters because they're each examples of heavily prioritizing one type of efficiency over the other, and with good reason in each case. There are numerous applications where a more equal balance is appropriate. The manufacturing industry largely looks to create balance between the two as best as possible, accepting that there are going to be inefficiencies at either end of the spectrum.
