---
layout: post
title: The Difference Between Velocity and Capacity
snippet: >-
  People often refer to velocity and capacity interchangeably. Velocity and
  capacity are both represented as something like "the amount of work the team
  can do in an iteration." However, I'd like to suggest that they are
  fundamentally different. 
category: Work
featured: false
enabled: false
ogDescription: >-
  People often refer to velocity and capacity interchangeably. Velocity and
  capacity are both represented as something like "the amount of work the team
  can do in an iteration." However, I'd like to suggest that they are
  fundamentally different. 
date: '2020-07-09 09:56:38'
---
People often refer to velocity and capacity interchangeably. Velocity and capacity are both represented as something like "the amount of work the team can do in an iteration." However, I'd like to suggest that they are fundamentally different. 

The above definition works fantastically when we're talking about capacity. Capacity is all the work that the team does related to the software development lifecycle in the course of an iteration. This would include backlog items that deilver business value, fixing bugs, tech debt, version upgrades, etc. Capacity is simple. It's all the work the team can do in an iteration.

Velocity, on the other hand, is a subset of capacity. It consists _only_ of the product backlog items that represent new functionality that was requested by stakeholders or users. In short, _velocity is the rate at which the team delivers business value_. 

This distinction becomes most important when bugs enter the team's sprint. Many teams want to include bugs in their velocity. However, a bug is a _failure_ to properly deliver business value. It is business value that was requested, but not properly delivered.

Separating velocity from capacity is beneficial because it allows the team to better forecast the rate at which they deliver business value. This, in turn, allows the business to better plan for the future. If the team and the business were to only look at capacity, or equate velocity with capacity, the business would expect more new functionality than is realistic. This would, however, disregard the work that isn't new functionality.



1. The Bug debate - to count them or not to count them
2. Distinguishing between capacity and velocity
   1. Why that distinction is important, e.g., prediction
   2. Better definitions for velocity and capacity
3. Back to bugs, how to count them
   1. Not part of velocity, but part of capacity
   2.
