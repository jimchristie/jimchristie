---
layout: post
title: The agile community is its own worst enemy
snippet: blah blah
category: Work
tags: []
featured: false
enabled: false
date: '2022-04-08 07:39:19'
syndications:
  - syndication:
      site-name: Bicycles Stack Exchange
      url: 'https://biycles-post-link.com'
  - syndication:
      site-name: LinkedIn
      url: 'https://linkedin-post-link.com'
---
I count myself in that title.

There's no question that the Agile field is an absolute mess. Every day I see people on LinkedIn sharing something that is just objectively wrong. Those shares inevitably get liked, commented on positively, and reshared, despite even the negative and corrective comments. New frameworks pop up all the time. Certifications grow at an exponential rate with the frameworks. Most of those certifications, especially the most common and sought after in job postings, have an incredibly low bar. We find ourselves in a place where anyone can call themselves a "master" or "coach" of whatever flavor of agility they happened to encounter first. These certified "masters" and "coaches" then immediately start sharing out their newly acquired and half-understood "knowledge" and the cycle repeats all over again. 

We have no one to blame but ourselves. Again, I include myself in that statement. 

### Overconfidence

Many new agilists want to share their newly acquired "knowledge." And there's no better outlet to do so than social media. In a way, it's great that they're so excited about what they've learned that they want to share it. Unfortunately there are too many times where they haven't validated what they've learned. Either they were taught by someone who didn't really understand the concepts themselves or they just haven't lived in the trenches long enough to truly understand. Or both. If you've heard of the [Dunning-Kruger Effect](https://en.wikipedia.org/wiki/Dunning%E2%80%93Kruger_effect), this probably sounds familiar.

Unfortunately, doesn't offer a lot in the way of filtering those who are truly knowledgeable from those who have overestimated their abilities. Sure, you could dig into a person's background, look at their job history, see if they've truly practiced what they're preaching, but who has time to do that? There's simply too many voices out there to do background checks on them all. 

### Judgment



### Silence

I've been involved in the agile space for about seven years now. It's not a terribly long time, but it's also a third of what most people might (mistakenly) call the entire history of agile. I was one of those people who shared some of those wrong ideas in my early days. You can see some of them in this blog. (I'm torn between taking them down for the good of the community and leaving them up to show how my thoughts have progressed through the years.) Nowadays, I'm fairly reluctant to share my thoughts online due to the knowledge that I'll probably learn something new relatively soon and regret perpetuating a wrong-headed idea.
