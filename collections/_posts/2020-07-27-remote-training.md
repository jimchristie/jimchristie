---
layout: post
title: Remote Training
snippet: >-
  Just before the onset of the Coronavirus pandemic, I found myself
  serendipitously working for [Pearson](/work/agile-facilitator/), a "remote
  first" organization. One of the things I was excited about at Pearson was that
  I was pretty sure that I would be able to continue the [Scrum training
  courses](/blog/the-first-of-many-two-day-training-sessions/) that I had
  previously given at [Shelter](/work/agile-coach-and-trainer/). I wasn't sure
  what those training sessions would look like in a remote first organization,
  but I had hoped to continue giving them in-person, in the same two day format
  to which I was accustomed. Covid-19 removed that possibility.
category: Work
featured: false
enabled: true
date: '2020-07-27 07:15:29'
---
Just before the onset of the Coronavirus pandemic, I found myself serendipitously working for [Pearson](/work/agile-facilitator/), a "remote first" organization. One of the things I was excited about at Pearson was that I was pretty sure that I would be able to continue the [Scrum training courses](/blog/the-first-of-many-two-day-training-sessions/) that I had previously given at [Shelter](/work/agile-coach-and-trainer/). I wasn't sure what those training sessions would look like in a remote first organization, but I had hoped to continue giving them in-person, in the same two day format to which I was accustomed. Covid-19 removed that possibility.

Instead, I recently found myself adapting that format to a remote environment. Needless to say, this was no small undertaking.

### Class Materials and Activities

The entire class revolved around physically building something using Scrum. There were also several side activities that involved some sort of physical game. I needed to find remote equivalents for as many of those as possible. I further needed to find a way to deliver the content for any games where I couldn't come up with a replacement activity. Finally, I needed a flexible way for the participants to interact with one another in a digital space, something similar to whiteboards and posters.

For the first part, I found a [pixel art spreadsheet](https://docs.google.com/spreadsheets/d/13bQaexKbu9Qyah1K-1ZAs6VF2TCRaUi6va-IH1gS2Ko/edit) that was designed for a team building game. I scrapped the suggested artwork and used the spreadsheet as a framework for the team to "build" in. With that taking care of the main brunt of the class, I was left to find similar activities for the rest. I settled on a [Mural board](https://www.mural.co/) for both those remaining activities, and the flexible interaction that I needed for participants. 

### Participant Engagement

While there were certainly some aspects that weren't as good as they would have been in person, there was at least one big benefit to being remote: I was able to split the class up in small sessions over the course of a week. This is something I could never do in person due to limited meeting room space and the inability to tear down and set back up every day.

Meeting in short sessions over the course of a week made it easier for people to fit the class into their schedules. Additionally, I was able to give small homework assignments so that participants had a chance to immediately apply what they were learning and then discuss it the following day.
It's also challenging to keep people engaged for two full days. In a two-day class, participants are visibly exhausted by the afternoon of the second day. It's especially hard to keep engagement up. However, by splitting the class up I'd say that participants were actually more engaged as the week went on.

All in all, the whole thing went better than I could have expected. The participants were very engaged and largely ranted and raved about the format, but still gave some constructive feedback. We had tons of conversations about relating the materials to the real world, just like we would in person. I couldn't have asked for a better first run.

### Future Classes

At least in the near term, I'm sure I'll be doing more remote classes. But even in the farther future, I'd say that I'm likely to keep bringing remote training sessions into the mix of what I can do. And as time goes on, I'll discover better and better ways to work around the limitations of remote training, as well as take better advantage of the opportunities it offers. 
