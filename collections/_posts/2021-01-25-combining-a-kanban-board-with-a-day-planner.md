---
layout: post
title: Combining a Kanban Board with a Day Planner
snippet: >-
  I mentioned [previously](//blog/50-ish-posts/) that one way to hold oneself
  accountable to a goal is to to create a plan with microgoals milestones, and
  such. As someone who is passionate about agility and the practices that
  support it, a kanban board was my knee-jerk thought. But what about another
  tool I like, my [Bullet Journal](https://bulletjournal.com/)? I bounced back
  and forth a bit between the two, but then it hit me: They're both flexible
  tools. Why not combine them?
category: Work
featured: false
enabled: true
ogDescription: >-
  I mentioned [previously](//blog/50-ish-posts/) that one way to hold oneself
  accountable to a goal is to to create a plan with microgoals milestones, and
  such. As someone who is passionate about agility and the practices that
  support it, a kanban board was my knee-jerk thought. But what about another
  tool I like, my [Bullet Journal](https://bulletjournal.com/)? I bounced back
  and forth a bit between the two, but then it hit me: They're both flexible
  tools. Why not combine them?
ogImage: /img/uploads/kanban-day-planner.jpg
date: '2021-02-05 01:45:06'
---
I mentioned [previously](//blog/50-ish-posts/) that there are a couple of pretty standard ways to hold oneself accountable to a goal. In particular, I was talking about New Year's resolutions. The first was to simply tell someone about the goal. In this post, I'll demonstrate how I'm approaching another way to hold myself accountable, by combining a kanban board and day planner to create a plan with microgoals milestones, and such. 

As someone who is passionate about agility and the practices that support it, a kanban board was my knee-jerk thought. But what about another tool I like, my [Bullet Journal](https://bulletjournal.com/)? I bounced back and forth a bit between the two, but then it hit me: They're both flexible tools. Why not combine them?

### The Flexibility of Kanban

Kanban is more flexible than many in the software world realize. Most people in software are familiar with kanban as a board consisting of a few columns. The simplest version has three, "To Do," Doing," and "Done." The "Doing" column can be split into any number of columns to represent the workflow of your software.

This board has its roots in the manufacturing world where kanbans are used, but in a much different way. In manufacturing, a kanban is a much more flexible indicator that some piece of work needs to be done. Here's one that most of us have probably seen outside of the manufacturing world:

![Starbucks cup showing kanban](/img/uploads/starbucks-kanban.jpg)

The cup itself is the kanban here. When a barista sees a cup sitting on the counter, they know they have to make some coffee. The checkboxes on the side indicate what kind of coffee needs to be made. Super simple! Much of the manufacturing world revolves around a similar flow. A kanban appears at a work station, and the work gets done. Or a supply bin is emptied to a point that exposes a kanban, and the kanban says how much to re-order. 

Thinking about that flexibility, I started looking to other sources to create a kanban board tailored to my use case. I took a bit of inspiration from [Personal Kanban by Jim Benson and Tonianne DeMaria Barry](https://smile.amazon.com/Personal-Kanban-Mapping-Work-Navigating/dp/1453802266/ref=sr_1_1?dchild=1&keywords=personal+kanban&qid=1612293607&sr=8-1). The book is about managing the day-to-day flow of our personal lives with a kanban board. In the book, they give an example of a kanban board that has a large section for regular tasks, and another section for breaking down large, ongoing projects such as writing a book. 

![personal kanban board with project area](/img/uploads/personal-kanban-example.png)

While I fundamentally liked the idea of a dedicated project area, I wanted to allow for a few projects and also incorporate their tasks into my daily workflow. This is where the bullet journal comes in. 

### Bullet Journaling

I've been Bullet Journaling for a few years now. It's a super flexible tool to both plan your days, weeks and months; as well as keep track of your thoughts, organize ideas, take notes, keep a diary, or whatever else you may need to write down. One of the core features of Bullet Journaling is to use a Daily Log to track your tasks. 

![Bullet Journal](/img/uploads/bullet-journal.jpg)

This Daily Log was easily the most used structure in my Bullet Journal. It became my default project tracking method when I worked in an office. It was small enough to be portable, flexible enough to handle whatever I threw at it, and I was able to apply a WIP limit by simply constraining the space that I left myself to add tasks. 

### Kanban Day Planner

With all of the fexibility of these tools, decided to attempt to combine the two ideas into my own, personal kanban day planner. 

![my kanban day planner](/img/uploads/kanban-day-planner.jpg)

The day planner part of my board is the Monday through Friday columns at the top. In effect, those are also rolling "to do" and "doing" columns. The bottle cap represents today, i.e., "Doing," and shows the tasks that I aim to complete today. Every other day is in the future, i.e., "To Do." For example, the above image was taken on a Tuesday. The column for Wednesday would be the very next day and the column for Monday represents Monday of the following week. When I woke up on Wednesday morning, I moved the bottle cap and any tasks as needed. At that point, Tuesday became Tuesday next week. This gives me the opportunity to always look a full week ahead. 

I essentially have my backlog divided into three areas. Quick Fixes and Medium Items are both for things that aren't connected to a specific project. Quick fixes are tasks that I can knock out fairly quickly, under an hour and ideally closer to fifteen minutes. Medium items are things that I expect to take a couple of hours. The Ongoing Projects area at the bottom shows (surprise!) ongoing projects. These are things that require multiple sessions over the course of days, weeks, or months. 

Everything is color coded so that I can easily recognize what's being planned and worked on, and what isn't. As you can see right now, my CSM Materials project is being neglected. It's probably safe to say that [somebody ](https://media.tenor.com/images/8b8619039f9e76eb10470a554742a748/tenor.gif)has too much WIP. (I'm hoping that the empty Medium Items column will stay empty and allow some of those CSM tasks to move up next week.)

### The tools should fit the flow, not the other way around

It's not my intention to say that I think a kanban day planner is the next evolution in project management. Rather, I wanted to illustrate that thinking of tools as infinitely flexible makes it possible come up with new ways of adapting them to a specific workflow. 

The problem I had with both the Bullet Journal and a kanban board is that neither really fit my workflow. I wanted to track my blog project, but also see how other work might get in the way of that, or vice versa. This board is super-specific to those needs. It also gives me that accountability that I was looking for. There's a specific, but flexible plan for how each post will progress and each week culminates in a blog post-  i.e., a microgoal - that leads to my larger goal of weekly-ish posts for the year. 

This probably won't be the final iteration of my kanban day planner. It'll evolve as I discover things about it that don't work as well as I'd like, or as I discover new techniques that might benefit me. And that's exactly how it should be.
