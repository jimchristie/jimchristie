---
layout: post
title: The Privilege of Being a White Man in 21st Century America
snippet: >-
  I am a white man living in American in the 21st Century. Those four facts
  about me offer an insight to the fortune and privilege that I experience for
  no just reason.
category: Life
featured: false
enabled: false
date: '2020-06-05 07:26:10'
---
I am a white man living in American in the 21st Century. Those four facts about me offer an insight to the fortune and privilege that I experience for no just reason. 

### Living in America

America is a first world country. This alone puts it, and all of its residents, a step above much of the world in terms of security, safety, prosperity, and opportunity. To live in a first world country is to live in a place where starving to death simply doesn't happen. It's to live in place where war rarely, if ever, reaches your home. It's to live in a place where one can learn virtually anything they desire simply by visiting a local library. 

Beyond that, America is one of the most prosperous and powerful countries on earth. We are the wealthiest country in the world. We dominate the tech space. We are a leader in the global political field. Our military is second to none. This means that not only do we, in this country, experience all of the benefits of living in a first world country, but we often experience the best of the best. 

### Living in the 21st Century

We often refer to the time that we're living in as the information age. This is a 
