---
layout: post
title: Measuring Boulders
snippet: >-
  Sometimes metrics are great. Sometimes you absolutely need them in order to
  evaluate something that you're doing. How well is my marketing campaign
  working? Was the change I made to my website effective, detrimental, or did it
  have no impact at all? 


  But there are also questions that may not _need _metrics to be answered.
  What's our biggest process impediment? How do customers feel about or service?
category: Work
featured: true
enabled: false
date: '2019-03-06 01:17:00'
---
A few weeks back, I read a [story about a rock slide](https://patch.com/colorado/golden/rockslide-closes-mountain-highway-near-glenwood-canyon-cdot) that closed a road in Colorado. The road was completely obstructed by boulders the size of a small car. This was a mountain pass and the detour through the nearest pass was 146 miles, which would take about three hours to drive. Obviously, this was going to be a problem for people who drove that road to and from work every day. 

People love numbers. Numbers give us a concrete set of data to evaluate, interpret, make decisions from, and justify our actions. Oftentimes, the first thing that someone asks when looking at a problem is, "What metrics do we have around that?" But that's not always the right question.

Sometimes metrics are great. Sometimes you absolutely need them in order to evaluate something that you're doing. How well is my marketing campaign working? Was the change I made to my website effective, detrimental, or did it have no impact at all? These are the types of questions that are nearly impossible to answer without metrics. 

But there are also questions that may not _need _metrics to be answered. What's our biggest process impediment? How do customers feel about or service? These are questions that we can answer just by looking around. 

When highway crews came upon the previously mentioned rock slide, they didn't stop and ask themselves "How big are these rocks? Can somebody get a scale? And maybe a measuring tape?" They just got to work clearing the road. 

Yet, we do that in our business flow all the time. We see an opportunity and immediately start asking for metrics. We want metrics around the current situation. We want metrics for different actions so that we can guesstimate which to take and know whether or not our path was successful. 

The road crew didn't have to measure success. They knew they were at least partially successful when they were able to reopen a lane. They could have sat down and applied an equation to determine how successful that lane reopening was. But they didn't need to. They knew they were successful because traffic was moving again, even if slowly. They knew the job was done because all the lanes were open. 

This is how we need to look at our metrics sometimes. Metrics are a type of self-inflicted opportunity cost. The gathering of metrics delays the implementation of action. Sometimes that opportunity cost is worth it. Sometimes, we're measuring boulders. We just need to be aware of when we are.
