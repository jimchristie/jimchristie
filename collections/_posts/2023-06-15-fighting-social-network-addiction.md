---
layout: post
title: Fighting Social Network Addiction
snippet: >-
  When Facebook first launched, I was _excited_. It seemed so cool! But then
  things started to change. 


  Other social network sites rose and fell. I abandoned Facebook, but not all
  social networks. And today, I struggle to manage my relationship with social
  networks.
category: Life
featured: true
enabled: true
featuredImage:
  fill: cover
  focalPoint:
    leftRight: center
    topBottom: center
  image: /img/uploads/social-media-addiction-midjourney.png
  orientation: landscape
ogDescription: >-
  When Facebook first launched, I was _excited_. It seemed so cool! But then
  things started to change. 


  Other social network sites rose and fell. I abandoned Facebook, but not all
  social networks. And today, I struggle to manage my relationship with social
  networks.
ogImage: /img/uploads/social-media-addiction-midjourney.png
date: '2023-06-15 08:23:39'
syndications: []
---
#### The Rise of Facebook

When Facebook first launched, I was _excited_. It seemed so cool! I was in college at the time and thought it seemed like an awesome way to interact with friends. I read all about it and eagerly awaited the approval for registrations from my school. As soon as I could, I signed up.

Over the next several years, I was an avid Facebook user. I posted the ridiculous shenanigans that I, as a college student, was up to. I got back in touch with old friends. I loved it. 

But then things started to change. 

People were having real world arguments over Facebook posts. The user base was growing. Employers were looking applicants up on Facebook before hiring. Now, I had to consider each post and comment as something that could actually impact my personal and professional life. I started culling and curating my content. Facebook created some privacy settings. I diligently and carefully set those.  

But that wasn't all. The rapidly growing user base had implications that I didn't anticipate.  It seemed rude to reject a friend request, no matter how trivial the real world connection was with the requester. My "friend" list was growing exponentially. This meant that my feed was filled with noise. Kids' birthday party announcements, pictures of what people ate for breakfast, hot takes on issues that I didn't care all about, and all sorts of things that I didn't care about from people I barely knew were the majority of what I saw. 

On top of all of that, Facebook suffered a series of security breaches. All those privacy settings that I had so diligently set seemed to mean nothing. 

Meanwhile, I'm sitting at my computer, scrolling through the now-endless feed of nonsense from people I barely know, hoping that I don't miss out on something that actually interests me from someone I actually care about. 

Once I realized it was time to let it go, it still took a long time to actually do it. The [FOMO](https://en.wikipedia.org/wiki/Fear_of_missing_out) was real. Eventually I did it though. I shut down my Facebook. It wasn't easy and it took a long time to get used to, but I was ultimately glad I did it.

#### Other Social Networks

Of course, Facebook wasn't the only player in the market. Twitter had its own niche. Google was trying (and failing miserably) to gain ground with [Google+](https://en.wikipedia.org/wiki/Google%2B). LinkedIn rose up as a business-oriented social network. I was on all of them at one point or another, like a junkie chasing that first high. 

#### The Last One Standing (for me)

Today, LinkedIn is the only social network service that I still use, and I have a troubled relationship with it.

I keep using LinkedIn simply because I work on tech. A LinkedIn profile is like a second resume in the tech field. When reviewing candidates, one of the first thing that most people do is look at the candidate's LinkedIn. It's also a great way to keep up with industry trends, share and evaluate new ideas, and generally grow in the field. 

It's also a great job hunting tool. I personally find it better than any of the other options out there. When I'm job hunting, I can hop on LinkedIn and apply for dozens of jobs easily that I'd otherwise have to visit who-knows-how-many sites to apply for. It also gives me the name of the job poster so there's someone I can contact directly about the job. I can even see any connections that might help me get a foot in the door. Add all of that to the fact that applying through LinkedIn basically guarantees that the hiring manager will look at my carefully curated LinkedIn page and it's a no-brainer. 

It's not all roses though. LinkedIn suffers many of the same drawbacks as other social networks. Curating your presence is work. It's socially awkward to reject a connection request. Feeds fill up with garbage. I find myself scrolling an endless feed to fill a idle times. It's still addictive. The FOMO is still real.

#### How I Manage

When I find myself when I going to the site without thinking about it, scrolling endlessly without much sense of reward, I change my password and don't save the new one. It's simple. It's easy. It's effective. This barrier helps to keep me from logging in and scrolling the endless feed. 

But I also know that, when the time comes, I'll be back. I'll have a good reason. It'll be time to look for a job. I'll go to a conference or training and meet some people that I want to connect with. As long as it's there, something will draw me back in. I'll click the "forgot password" link, get a new password, log back in, and be hooked again. 

I'd like to find a way to break the cycle completely, but LinkedIn has found a place in the world where it's a necessary evil. At least, it feels that way in my world.
