---
layout: post
title: Conflicting Values in the Agile Manifesto
snippet: 'Yup, need one.'
category: Work
featured: false
enabled: false
date: '2021-04-15 07:52:10'
---
> Our highest priority is to satisfy the customer through early and continuous delivery of valuable software.

And also

> Working software is the primary measure of progress.

If satisfying the customer is our highest priority, why isn't that also the primary measure of progress? Perhaps the value should be: 

> The value of the software delivered is the primary measure of progress.
