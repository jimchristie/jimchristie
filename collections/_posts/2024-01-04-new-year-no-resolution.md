---
layout: post
title: 'New Year, No Resolution'
snippet: >-
  I've written a
  [couple](/blog/smart-goals-to-make-your-new-year-s-resolutions-successful/)
  times before about [New Year's Resolutions](/blog/50-ish-posts/). Like most
  people, I suck at them. Even after following my own advice, I've repeatedly
  fallen into failure. That's why I'm trying something different this year.
tags:
  - tag: Life
featured: true
enabled: true
ogDescription: >-
  I've written a
  [couple](/blog/smart-goals-to-make-your-new-year-s-resolutions-successful/)
  times before about [New Year's Resolutions](/blog/50-ish-posts/). Like most
  people, I suck at them. Even after following my own advice, I've repeatedly
  fallen into failure. That's why I'm trying something different this year.
date: '2024-01-04 09:37:52'
---
I've written a [couple](/blog/smart-goals-to-make-your-new-year-s-resolutions-successful/) times before about [New Year's Resolutions](/blog/50-ish-posts/). Like most people, I suck at them. Even after following my own advice, I've repeatedly fallen into failure. That's why I'm trying something different this year. 

I'm doing two things that will hopefully help me to make some simple improvements in my life, but aren't full on "resolutions." And really, that's what those New Year's Resolutions are about, right? We're just trying to improve ourselves.

The first thing I'm trying was inspired by [Atomic Habits](https://jamesclear.com/atomic-habits) by James Clear. It's an excellent book that I've used to help me improve various aspects of my life. The book makes a couple of points that I'm combining into a single practice. The first is that improving by 1% every day results in a (roughly) 38 fold improvement in a year. 

![Graph showing 1% better and 1% worse every day. 1% better results in a 37.78 fold increase. 1% worse results in 97% decrease. ](/img/uploads/tiny-gains-graph-700x700.jpg)

<div class="image-caption">
Image credit: [James Clear](https://jamesclear.com/continuous-improvement).
</div>

The second point that James Clear makes is that 1 push-up is better than no push-ups. Essentially, it's better to do something than nothing. When we try to establish a new habit, we often go through a mental exercise when we fail, even marginally. "I don't have time to do an hour at the gym today, so I'll just skip it." But that's silly. 15 minutes at the gym is better than not going at all. And if our goal is to get in shape, one push-up is better than nothing. 

So I've mashed those two ideas together and created a [spreadsheet](https://docs.google.com/spreadsheets/d/1yeICPT9MLsX3IhOImOOQGrXM0o5d_9hcAqXECHHyfrE/edit?usp=sharing) that will increase the number of push-ups I do each day by 1%. Naturally, it's impossible to do 1.01 push-ups, so I let it round. The hope is that the push-ups will just get easier and easier each day and that by the time I add one, it'll be no big deal. 

I've also added a single lunge on each side and 5 crunches in as a starting point. This keeps the starting point for my new exercise habit at under 20 seconds and will give me a reasonably good 5-minute jump start workout by the end of the year. It'll be 38 push-ups and lunges and 189 crunches. 

Fingers crossed. 

The second thing I'm trying is only for the month of January. My wife and I have been discussing a lot lately how we get sucked into TV. It's so easy to just watch "one more" show. Unfortunately, there's always one more. We've tried a few things over the last year or two to curb our TV watching, but it always slips back. We've also discussed just canceling all of our streaming subscriptions and cutting it out entirely, but that feels extreme. 

So what we've decided to do is cut it out completely for the month of January. We've given ourselves two exceptions:

1. TV is ok when we're exercising. We ride indoor cycling trainers some in the winter, as well as watching workout videos. As long as we're on a bike or working out to a video, the TV is ok.

2. TV is ok when other people are involved. We play video games and watch TV with family from time-to-time. Since those are social bonding times and are naturally limited by the group's availability, the TV is ok. 

This should let us feel the pain of quitting an addiction, but also be a small enough period of time that it's relatively easily achievable. 

The main goal is just to experience it and learn from it. How terrible is it? What else will we fill our time with? Is it something that we would like to make permanent, or do we need to look for new ways to limit our TV watching? Or maybe we find that we enjoy our time so much more that we're not tempted to watch much TV anymore. Wouldn't that be something? 

So that's what I'm trying this year instead of the standard New Year's Resolution. I'll try to follow up as time passes. Who knows? Maybe writing more in my blog will be the thing that replaces TV!
