---
layout: post
title: Near Shore Teams
snippet: >-
  My wife and I were chatting recently about how neither one of us can
  understand why organizations seem to _love_ hiring offshore team members. It
  never seems worth it. Even if they cost half of what a more-or-less local team
  member would cost, the overhead in managing that dependency more than eats up
  those savings.
category: Work
featured: false
enabled: true
date: '2024-05-22 03:11:07'
---
My wife and I were chatting recently about how neither one of us can understand why organizations seem to _love_ hiring offshore team members. It never seems worth it. Even if they cost half of what a more-or-less local team member would cost, the overhead in managing that dependency more than eats up those savings. 

The problem is, of course, time zone differences. When team members need to collaborate with one another, but have very little overlap in their day, it severely limits that collaboration. One team member is constantly slowed down or stopped entirely because they're waiting on the other. Of course, team members can work on other things, but then they're focusing on lower priority items at the sake of what's most important.

There's nothing wrong with team members spanning two, three, even four time zones. Assuming that everyone works eight hour days, four time zones starts to push the limits of effective collaboration, but it's doable. Anything more than that seems to become a problem. And when workdays don't overlap at all, or only overlap by an hour or so, it's a real problem. 

And I want to be explicitly clear here: this doesn't mean that you can't have a multi-cultural workforce. Most time zones in the world span multiple cultures. And if your team spans two or three time zones, you have an ample range of cultural backgrounds that you can attract team members from.

This also doesn't mean that a company can't have a globally distributed workforce. What it does mean is that teams and team members should be mostly decoupled from other teams and team members who are separated by four or more time zones. What is meant by "mostly decoupled" is probably something that needs to be defined on an case-by-case basis. However, it's safe to say that if team members need to interact daily, they shouldn't be several time zones away from each other. I'd even go as far as to say that weekly or monthly interactions are probably too frequent for offshore interactions. 

If you're building a team, or reorganizing teams, I implore you: don't set people up for offshore collaboration. It's a huge pain for employees and has a real monetary impact to the company in the form of opportunity cost. 
