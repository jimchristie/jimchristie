---
layout: post
title: Maximum Acceptable Shoddiness
snippet: >-
  MVP, or Minimum Viable Product is a term that gets thrown around business like
  crazy. It's especially ubiquitous in "Agile" contexts. MVP is something that
  we constantly ask ourselves. What is the MVP? What can we put off until Day
  Two? What are the need-to-haves and what are the nice-to-haves? These are all
  great discussions to have. 


  However, sometimes, these conversations change.
category: Work
featured: true
enabled: true
ogImage: /img/uploads/frustrated-dev.jpg
date: '2019-06-14 10:28:00'
---
MVP, or Minimum Viable Product is a term that gets thrown around business like crazy. It's especially ubiquitous in agile contexts. MVP is something that we constantly ask ourselves about. What is the MVP? What can we put off until Day Two? What are the need-to-haves and what are the nice-to-haves? These are all great discussions to have. This is because these conversations allow us to get something in front of the customer. It lets us get their reactions and informs us whether or not we're on the right track. If we _are_ on the right track, it brings us a return on our investment faster. These are all great things.

However, sometimes, these conversations change. It's usually when we're well into software development. The features have been defined, designed, and mostly built. We discover a problem and start talking about whether or not to release anyway. Someone invariably asks whether or not fixing it is MVP. 

At this point, the conversation has subtly shifted from asking what features should be released to asking what level of quality is expected. The implication is that MVP goes beyond which features should be released and encompasses how well those features should work. By this definition of MVP, a component of MVP is the minimum level of quality that we will accept. 

Now, there's nothing wrong with discussing and enforcing a minimal level of quality. But there's an important thing to keep in mind: The concept of a minimum level of quality implies that there is also a Maximum Acceptable Shi... shoddiness, MAS, if you like.

![Minimum quality vs. maximum shoddiness illustration](/img/uploads/quality-illustration.gif)

If our quality discussions are part of our MVP discussions, we'll discover that all products should be released with Maximum Acceptable Shoddiness. We should cut all the corners that we think we can get away with for the sake of getting the product out the door. Not only should we let problems slip through the cracks, but we should be actively looking for ways to cut corners. Just the same as with focusing on the right features, focusing on maximum acceptable shoddiness lets us get our product out the door faster. We can test our assumptions and start getting that ROI even faster. 

The problem is that this practice would lead us to routinely release garbage. 

MVP discussions are hard to have and the subsequent decisions are hard to make. But they're always the right conversations to have. When we have conversations about what the right features are, we can all pat ourselves on the back and congratulate ourselves for exercising real agility. We should always feel comfortable having those conversations.

Conversations about sacrificing quality, on the other hand, should always feel uncomfortable. Sacrificing quality is, by definition, a problem. It might be a solution to a bigger problem, but it's still a problem in itself. By disguising these discussions as MVP discussions, we've removed, or at least lessened, the discomfort of the discussion. That's a bad thing.

Sometimes we feel pressure to get something out. We've been struggling to meet our velocity, we're up against a deadline, the higher-ups are getting restless, whatever. We're looking to get something in front of customers and sacrificing quality feels like the right thing to do. Maybe this really is a discussion that needs to be had. Maybe sacrificing quality really is the right thing to do. (Hint: It's probably not.)

If we need to have discussions about sacrificing quality, let's have them. But let's not disguise them as a good thing. Let's address them as they are: a discussion about making the best of the bad situation. This brings the underlying failures front and center, so that we will, hopefully, address them and do better next time. After all, that's part of what agility is all about.
