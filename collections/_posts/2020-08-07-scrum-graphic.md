---
layout: post
title: Scrum Graphic
snippet: >-
  Recently, I was preparing for a Scrum class that I would be teaching remotely.
  Since I'm accustomed to doing training sessions in person, I'm used to drawing
  posters for my classes. However, this wasn't going to work anymore and, as a
  result, I found myself in need of a good graphic of the Scrum Framework. 


  So I made my own. If you find it useful, please use it!
category: Work
featured: true
enabled: true
featuredImage:
  fill: contain
  image: /img/uploads/scrum-framework.jpg
  orientation: landscape
ogDescription: >-
  Recently, I was preparing for a Scrum class that I would be teaching remotely.
  As a result, I found myself in need of a good graphic of the Scrum Framework.
  So I made one. If you find it useful, please use it!
date: '2020-08-07 11:24:10'
license: Creative Commons Attribution-ShareAlike 4.0 International
---
Recently, I was preparing for a Scrum class that I would be teaching remotely. Since I'm accustomed to doing training sessions in person, I'm used to drawing posters for my classes. However, this wasn't going to work anymore and, as a result, I found myself in need of a good graphic of the Scrum Framework. 

A quick [Googling](https://www.google.com/search?q=scrum+framework&safe=off&sxsrf=ALeKk00PXmtxAcdAE-dructFKF9J6hpYRw:1596817637419&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjinbKgwYnrAhXrlHIEHcQJBywQ_AUoAXoECBQQAw&biw=1920&bih=937) let me know that I had plenty to choose from. Unfortunately, I wasn't finding any that quite fit the criteria I had in mind. I wanted an image that showed the roles, artifacts, and events mentioned in the Scrum Guide using language from the Scrum Guide, and nothing else. I was surprised to discover that every image I looked at left something out or unlabeled, added something extraneous, or used colloquial language such as "stand-up" instead of "Daily Scrum."

So I made my own.

I'm not such a purist that I think people must do Scrum and nothing but Scrum. However, in a training context, I want to be clear on which terms mean what, and what's part of the core framework and what's not. This just makes it easier to deliver a consistent message that is easy to research after the class. If I'm teaching one thing, and the class participant researches and finds something else, it's confusing. 

Clearly, there are things about Scrum that aren't included here. I've left out the five Scrum values, the three pillars of empiricism, and Definition of "Done," recommendations about team size, meeting timeboxes, and probably some other stuff. For my purposes in the class, these weren't necessary, they came at a different point in the session. I just wanted a really high-level picture of the nuts and bolts of Scrum. 

If you find this image useful, please use it! That's the whole reason I'm sharing it. I've licensed it as [Creative Commons 4.0 Attribution-ShareAlike](https://creativecommons.org/licenses/by-sa/4.0/) so that it will, I hope, be as useful as it can possibly be.
