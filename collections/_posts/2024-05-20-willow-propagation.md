---
layout: post
title: Willow Propagation
snippet: >-
  I've been wanting to try propagating willow trees for quite some time.
  According to [some internet
  searching](https://www.google.com/search?q=willow+propagation), it's super
  easy. Long term, I'm hoping to build a [living
  fence](https://www.google.com/search?q=willow+living+fence). I took the first
  steps over the weekend.
category: Life
tags:
  - tag: trees
featured: true
enabled: true
featuredImage:
  fill: contain
  image: /img/uploads/willow-cuttings.jpg
  orientation: portrait
date: '2024-05-20 11:36:26'
---
I've been wanting to try propagating willow trees for quite some time. According to [some internet searching](https://www.google.com/search?q=willow+propagation), it's super easy. You basically just take some cuttings, stick them in water, wait for roots to show up, and then plant them. Different sources have different instructions on how long to make the cuttings, when to take the cuttings, how thick the cuttings should be, and whether they should be green or fully hardened. In a nutshell, it doesn't matter. It just _works_. I have a couple of willow trees, so I got some cuttings from one and started them yesterday.

![small cedar tree in a pot](/img/uploads/rescued-cedar.jpg)

I also rescued a little cedar tree over the weekend that was growing up against a a bigger tree and didn't have much chance of making it to maturity. Unfortunately, I damaged the roots pretty badly when I was digging it up. They were all tangled up with the roots of the larger tree. An added bonus to the willow propagation is that the willow cuttings release rooting hormone into the water as they grow. Since I'll be storing mine outside, I'll need to empty the water every few days to prevent mosquitoes from using it as a breeding ground. I'll empty the water with the rooting hormone into the pot of my little rescue, helping it to recover from being transplanted. Win win!

Long term, I'm hoping to build a [living fence](https://www.google.com/search?q=willow+living+fence). You plant the trees relatively close together and then twist or tie their branches together as they grow. If you plant them at an angle, you can weave the actual trunks together. They'll eventually graft onto one another and basically become one big, long, fence-shaped tree. If you don't want it to get too big, you can trim them in the fall and each spring they'll grow a big canopy of leaves, which you just trim back again in the fall. 

I've been pretty intrigued by the idea of a living fence for a while now. If these cuttings take off, they should give me a good head start in that endeavor. I'm really excited about it!
