---
layout: post
title: POSSE Content Strategy
snippet: >-
  There are a few different circumstances that are coming together in my life in
  the moment that have made me reconsider the way I approach the content I
  create and put on the internet.


  As such, I'm piloting a [POSSE content
  strategy](https://indieweb.org/File:POSSE-2012-312.jpeg).
category: Life
featured: true
enabled: true
featuredImage:
  fill: contain
  focalPoint:
    leftRight: center
    topBottom: center
  image: /img/uploads/posse-2012-312.jpeg
  orientation: landscape
ogDescription: >-
  There are a few different circumstances that are coming together in my life in
  the moment that have made me reconsider the way I approach the content I
  create and put on the internet.


  As such, I'm piloting a [POSSE content
  strategy](https://indieweb.org/File:POSSE-2012-312.jpeg).
ogImage: /img/uploads/posse-2012-312.jpeg
date: '2023-06-30 02:09:39'
---
There are a few different circumstances that are coming together in my life in the moment that have made me reconsider the way I approach the content I create and put on the internet.

#### A push toward a unified online presence

I've been working for a long time to create a unified online presence. I diligently copy/paste content across various sites that I use all over the internet. I've also moved away from using user IDs that obscure my real name. But still, my content remains fragmented around the web. A user in one place doesn't necessarily have a way to find other content by me elsewhere.

#### The Desire to Monetize

Let's face it: We all gotta eat. Don't worry though, I'm not going to put my content behind a paywall or plaster my site with ads. Rather, I've thought for a while about incorporating affiliate links into my site. It's definitely not a done deal. I'm not even entirely committed to it, but it's something I think about. 

#### A desire to write more

It occurred to me recently that in spite of a [goal of writing more often](/blog/less-focus-more-writing/) (one that you wouldn't think I've achieved based on this site), I actually do write a fair amount. It's just not always here. It's fragmented across the internet. 

I used to write more across different sites, but I've been [trimming my social media presence back](/blog/fighting-social-network-addiction/) over the years. Some of that is because I get sucked in, but some of it is because of the fragmentation of my online persona. 

#### Content fragmentation _de-incentivizes_ me from writing

The privatized nature of much of the internet has led me to pull away from publishing a lot of content on sites other than my own. Many sites wall off "their" content so that you have to be logged in to use it. Indeed, I put the word "their" content in quotes because in some cases, they effectively own the content that you wrote. This is especially true if you don't publish your content elsewhere. 

There's also the risk of a company changing its business model in a way that no longer supports your beliefs. As I've mentioned before, Facebook became untenable for me. The same is true for Twitter. 

#### The Recent (and past) Stack Exchange Struggles

Currently, one of the last bastions of my online presence, [Stack Exchange](/blog/it-s-all-about-moderation/), is going through [struggles](https://meta.stackexchange.com/questions/389811/moderation-strike-stack-overflow-inc-cannot-consistently-ignore-mistreat-an) that have me re-thinking my relationship to them. I once viewed them as a community-driven utopian network of Q&A sites, but the recent issues have started to reveal a dystopian underbelly. 

As a moderator on one of their sites, I felt duty-bound to have a good grasp of the issues. While taking it all in, I found myself down a rabbit hole of past bad behaviors. I discovered that Stack Exchange has had recurring incidences of major dissatisfaction in their communities almost as long as I've been a moderator. I've likely only missed it due to the small and relatively obscure nature of Bicycles Stack Exchange. 

#### A POSSE Strategy

It was through my research of the history of issues within the Stack Exchange communities that I discovered [a post](https://retrocomputing.meta.stackexchange.com/questions/1202/the-stack-exchange-data-dump-has-been-turned-back-on-yay) referencing a [POSSE content strategy](https://indieweb.org/POSSE). 

POSSE stands for Publish (on your) Own Site, Syndicate Elsewhere. The basic idea is that you publish content on your own website _first_ and then share it through other services like social networks. You could share a post on one site, or across multiples. The two important details are getting it on your own site first so that you definitively own the content and linking back to the original post from sites you've share it on.

This strategy helps me to address all of the above issues that have been brewing in the back of my mind. 

It helps me to standardize my online persona since all of my content will be centralized in one place and all of the syndicated content will point back to the source. That centralization, in turn, will incentivize me to write more (I hope). 

It also allows me to monetize my content, should I decide that I really want to go that way. I can use affiliate links on my own site and perhaps make a little money from people reading my posts.

#### Potential Downsides

Of course, it's not all roses. Nothing is perfect.

One potential, almost certain, downside is maintenance. Each post will have to be written on my site and then copy/pasted to the syndicate sites. This'll be a pain. I'm also in the progress of building custom post types and features to show when a post is syndicated and how that may affect licensing of a given post. (Stack Exchange, for example, requires a [Creative Commons](https://creativecommons.org/) license.) There will probably be other unforeseen maintenance issues.

Some sites also don't appreciate links back to a personal site, and perhaps disallow them altogether. It's almost certainly a gray area within Stack Exchange and is probably something that would be evaluated on a site-by-site basis. I've also heard that some Reddit communities will ban you for it without any prior warning. So we'll see how that goes. 

Despite the potential downsides, I'm pretty excited about the overall strategy. The worst case scenario is that I just quit and go back to less and less participation in online communities. So, what's to lose?
