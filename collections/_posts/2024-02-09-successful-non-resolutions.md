---
layout: post
title: Successful Non-resolutions
subtitle: ''
snippet: >
  As I mentioned before, I tried something a little different this year
  regarding self-improvement in the new year. Specifically, I told myself that I
  wouldn't watch TV for the month of January and started what is likely the most
  gentle exercise ramp-up ever devised. 


  I'm happy to report that both have been resounding successes.
category: Life
featured: false
enabled: true
ogDescription: >
  As I mentioned before, I tried something a little different this year
  regarding self-improvement in the new year. Specifically, I told myself that I
  wouldn't watch TV for the month of January and started what is likely the most
  gentle exercise ramp-up ever devised. 


  I'm happy to report that both have been resounding successes.
date: '2024-02-09 08:53:28'
---
As I mentioned before, [I tried something a little different this year regarding self-improvement in the new year](/blog/new-year-no-resolution/). Specifically, I told myself that I wouldn't watch TV for the month of January and started what is likely the most gentle exercise ramp-up ever devised. 

I'm happy to report that both have been resounding successes.

My wife joined in both experiments with me and we were easily able abide by our No-TV Rules. In fact, we quickly filled the time that we normally spent staring at the TV with more productive and/or more engaging activities. Before the month was out, we found ourselves wondering how we ever made time for TV in the first place. 

Our two exceptions proved to be useful as well. Our nephew wanted to watch some show with us and we did so guilt free. We put on some reruns while riding the trainers, which was kind of nice. But for the most part, we just didn't really watch TV. 

On the exercise front, I printed out the first month-ish (it was more work than I wanted to invest to print exactly a month) of my [spreadsheet](https://docs.google.com/spreadsheets/d/1yeICPT9MLsX3IhOImOOQGrXM0o5d_9hcAqXECHHyfrE/edit#gid=274448015) and set it on the coffee table, where I'd walk past it every morning when taking care of the dogs. I saw it, did my exercise, and went about my business. Once the habit was somewhat ingrained, my wife moved both of ours to a wall. 

![calendar showing days that exercise was completed](/img/uploads/exercise-calendar.jpg)

As you can see, I missed a few days at the beginning of the month. I was traveling those days and simply forgot because the habit wasn't ingrained yet. I'm reasonably confident that the next time I travel, I'll just get up and do my exercise. 

While looking at this calendar isn't very impressive, and I'm not doing an amount of exercise that's going to change my life, I'm super excited to be establishing a habit. By the end of the year, I'll be doing almost 200 crunches, 37 push-ups, and 37 lunges. While that's still not a life-changing amount of exercise, it's a great way to jump start the day and something that I can build upon if I choose to do so. 

What's more, the slow ramp-up makes establishing a long-term habit of exercise much more likely to succeed than the usual approach to simply starting to exercise for an hour a day. That pretty much always fails. At least, it always has for me. I've spent years repeatedly trying and failing to establish an exercise routine that way. If it takes two or three years for this to grow into a life-changing amount of exercise, so be it. It'll be much better than trying and failing for several more years.
