---
layout: post
title: People as Resources
snippet: >-
  Last week I did something that I generally try to avoid doing: I referred to
  people as resources. I don't want to belabor why here, that discussion has
  been well covered on the internets. The gist is that lumping people into the
  same category as other resources dehumanizes people. What I'd like to do
  instead is talk about when I think it's appropriate and when it's not.
category: Work
featured: true
enabled: true
featuredImage:
  image: /img/uploads/three-women-working-on-couch.jpg
  orientation: landscape
ogDescription: >-
  Last week I did something that I generally try to avoid doing: I referred to
  people as resources. I don't want to belabor why here. What I'd like to do
  instead is talk about when I think it's appropriate and when it's not.
date: '2021-02-19 01:23:06'
---
Last week I did something that I generally try to avoid doing: [I referred to people as resources](/blog/the-efficiency-of-firefighters-and-doctors/). I don't want to belabor why it is that I try to avoid that. That discussion has been [well covered on the internets](https://www.google.com/search?q=people+as+resources&oq=people+as+resources&aqs=chrome.0.69i59j0i10l4j69i60l3.3502j0j4&sourceid=chrome&ie=UTF-8). The gist is that lumping people into the same category as other resources dehumanizes people. What I'd like to do instead is talk about when I think it's appropriate and when it's not. Being a little careful with our language can largely mitigate the potential pitfalls. 

There are lots of people who argue that we should _never_ refer to people as resources. I'm clearly not in that camp. When we look at a complex system in which the goal is accomplishing some task or achieving some goal, any people contributing to that effort are _technically_ resources. There's really no good way to discuss how the system functions without referring to people as resources. 

I think the main criteria that we should apply when choosing whether or not to refer to people as resources is simply whether or not we can refer to them as people and still make sense within the context of the conversation. If we can use the word "people" without creating confusion, we should. It's incredibly easy to do and avoids the potential pitfalls of calling them resources.

In last week's post, it wouldn't really have made sense to talk about how doctors and firefighters are people in a system, because so are the people that they're servicing. It was an important distinction within that discussion who is acting as a resource and who is not. I can't see any way to carry on that conversation without referring to people as resources. 

Conversely, when we're in the workplace talking about people on projects, people often say something like "we need more resources" or "we need to move some resources around." In both of these applications, we could easily refer to people as people. Doing so would help us to keep from unintentionally dehumanizing those people. 

In the end, that unintentional dehumanization is the thing we're trying to avoid. Let's choose the times that we refer to resources carefully. Let's further keep in mind the possible negative side effects of doing so, and go through the mental exercises of mitigating those effects when we need to refer to people as resources. In our workplaces, we're all technically resources. And we'd all rather be treated as people.
