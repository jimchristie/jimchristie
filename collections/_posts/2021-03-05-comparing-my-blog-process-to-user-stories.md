---
layout: post
title: Comparing my blog process to user stories
snippet: yup
category: Work
featured: false
enabled: false
date: '2021-03-05 12:55:43'
---
Outline === spike

Rough draft === potentially shippable increment

second draft === another increment

images === gold plating

release === release

continuous delivery == content environment, feature flags
