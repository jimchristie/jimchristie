---
layout: post
title: There will always be more work than people
snippet: >-
  I [mentioned a couple weeks ago](/blog/people-as-resources/) that something I
  often hear at the workplace is that we need more ~~resources~~ people. I hear
  it over and over again, in every software development organization that I've
  ever worked in. So why is this? Why don't we ever have enough people to do
  everything on our plates? 
category: Work
featured: false
enabled: true
ogDescription: >-
  I [mentioned a couple weeks ago](/blog/people-as-resources/) that something I
  often hear at the workplace is that we need more ~~resources~~ people. I hear
  it over and over again, in every software development organization that I've
  ever worked in. So why is this? Why don't we ever have enough people to do
  everything on our plates?
date: '2021-03-12 07:04:32'
---
I [mentioned a couple weeks ago](/blog/people-as-resources/) that something I often hear at the workplace is that we need more ~~resources~~ people. I hear it over and over again, in every software development organization that I've ever worked in. It's usually in response to a feeling of being overloaded with work. So why is this? Why don't we ever have enough people to do everything on our plates? 

An aspect of software development work is that everything we deliver to users generates new ideas, opportunities, and even problems to solve. All of these ideas, opportunities, and problems require more work to address them. That new work, generates more ideas, opportunities, and problems. 

What's more, this cycle doesn't grow work in a linear fashion. That growth is exponential. Every new release doesn't create one new piece of work. It creates several, perhaps dozens, maybe more. In fact, the _best_ releases are also the ones that create the most new work. 

Certainly, adding more people will allow us to do more work, release more features. But it will clearly never be enough. Those new people will simply add fuel to the organization's ability to churn out work, creating even more new work. 

A different solution, and one we should look to first, is to revisit our lean and agile ideas and principles. Is our priority correct? Is our organizational WIP too high? Can we cut our scope? Asking ourselves questions like these can lighten our workload before resorting to adding more people. 

What's more, adding new people always slows things down in the short term. It takes time to train a new person. Any new person is a net _loss_ in productivity at first. It takes time to onboard that person, time that existing people would otherwise be working on the problem at hand.  

Growing the organization not only slows things down in the short term, it also creates more overhead in the long term. A larger team has more communication paths. More teams require cross team coordination. Suddenly, we have project management meetings that are taking people's time away from doing the actual work. 

Adding those additional people certainly create more organizational bandwidth, but those gains come at diminishing returns, at least to some degree. If working software is the primary measure of progress, an organization of 1000 people will certainly do more work than an organization of ten, but probably not a hundred times as much. 

Certainly organizations need to grow. The behemoths on the Fortune 500 list can't get away with 10 employees. That growth shouldn't be a knee jerk reaction to how much work we have at the moment. Rather, it should be part of a long-term strategic vision.
