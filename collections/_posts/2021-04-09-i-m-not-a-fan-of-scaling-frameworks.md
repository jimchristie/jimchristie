---
layout: post
title: I'm not a fan of scaling frameworks
snippet: 'Yup, need one.'
category: Work
featured: false
enabled: false
date: '2021-04-09 01:33:39'
---
While I've certainly not seen every scaling framework in existence, nor have I seen them applied at every company in the world, I'm becoming more and more convinced that they're rarely beneficial. Indeed, it seems to me that they're more often destructive. 

1. Intro ^
2. Orgs are unique
   1. Big ones
   2. Little ones
   3. Highly regulated ones
   4. Completely unregulated ones
   5. Lives are literally at stake in some
3. Why does Scrum work?
   1. Teams are more homogenous (probably not the right word)
   2. Similar size
   3. Cross-functional
4. Conclusion?
   1. Feels like something is missing
