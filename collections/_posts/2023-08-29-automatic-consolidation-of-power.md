---
layout: post
title: Automatic Consolidation of Power
subtitle: ''
snippet: >-
  A Dungeon Master has a lot of authority within a game of Dungeons & Dragons.
  What's interesting is how a DM accumulates additional power and authority. In
  most games, the DM also ends up taking care of scheduling, collecting money
  for and buying snacks, sending out reminders about upcoming games, and pretty
  much anything else that one might associate with leadership within a group.
category: Work
featured: false
enabled: false
date: '2023-08-29 10:01:30'
---
A few years back, my coworkers were playing Dungeons & Dragons on their lunch break. I was intrigued and asked to join in the game. I was pretty quickly hooked and started running games myself as a [Dungeon Master](https://en.wikipedia.org/wiki/Dungeon_Master). 

If you're not familiar with the game, it's a story-telling engine as much as it is a game. Players pretend to be fantasy characters and engage in a fantasy world. They can do literally _anything_ in the world, provided that it fits within the rules of the game. One player acts as the Dungeon Master (DM for short), a special role within the game. The DM has two primary jobs:

1. React to the player's choices. The DM plays all the other characters in the world, both friends and foes. He or she determines how these characters respond to the player's actions. 
2. Act as a referee when how to apply the rules isn't immediately obvious. Since the game is extremely open-ended, it is often unclear how a rule is applied, which rule applies, and even if a rule should be ignored.

These two roles mean that the Dungeon Master is in a position of extreme power within the game. It's common for people to say that the DM "runs" the game. A Dungeon Master could conceivably decide that a meteor shower obliterates the entire city that the players are in and the game would be over. This would, of course, not be cool and no DM would ever ruin a game like this. But it shows just how much power the DM has within the game. 

What's interesting is how a DM accumulates additional power and authority. In most games, the DM also ends up taking care of scheduling, collecting money for and buying snacks, sending out reminders about upcoming games, and pretty much anything else that one might associate with leadership within a group.
