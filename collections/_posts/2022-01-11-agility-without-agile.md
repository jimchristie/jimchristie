---
layout: post
title: Agility Without Agile
snippet: >-
  My first job in the software world was as a front-end developer at my [local
  public library](https://www.dbrl.org/). I had been working there in a variety
  of roles and had taught myself to code. When a part time development job
  opened up, I applied and got it. What I didn't know at the time was that this
  was the purest agile software development environment that I I could imagine.
category: Work
featured: false
enabled: false
date: '2022-01-11 07:28:58'
---
My first job in the software world was as a front-end developer at my [local public library](https://www.dbrl.org/). I had been working there in a variety of roles and had taught myself to code. When a part time development job opened up, I applied and got it. 

I didn't have any agile experience. In fact, when I stumbled across the idea of agility, I pretty quickly got the idea that it was something that teams needed to concern themselves with, something about specific meetings and collaboration. Since I was a team of one, I didn't dig any further. 

My responsibilities mostly entailed the various web tools that were used internally. Since I had access to my end users, I constantly asked them about the stuff that I was working on. After building a new feature or tweaking an existing one, I'd get up from my desk, grab an employee who I knew would be using it, and ask them for feedback. 

I did this with everything. 

There was a nearly finished application that I inherited from my predecessor. Even though I had been the target user for this app in my old position, I had never seen it. Before I finished it up, I went around and asked people about it. I scrapped a few things and added a few more in based on their feedback. 

I had an idea for a new app that would replace a paper system. I built a super simple prototype, showed it to a few people, and asked if they thought they'd use it. As I polished it up, I showed it to whoever I could find along the way. I made sure to include power users, and people who struggled with technology. I included supervisors who would need to sign off on switching to a new system. I incorporated their feedback. 

What I didn't know at the time was that this was the purest agile software development environment that I I could imagine. There were no sprints, no retros, no POs, none of the things that have become synonymous with "Agile." It was just a developer working directly with end users. 

In the years since then, I've often looked back at that job and thought to myself that the whole community of agilists is trying to recreate that experience, but with teams and larger organizations. We're trying to figure out how to get developers working with, or closely to, end users as often as possible. We're trying to tighten that feedback loop.
