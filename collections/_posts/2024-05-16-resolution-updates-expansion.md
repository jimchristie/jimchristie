---
layout: post
title: Resolution Updates & Expansion
snippet: >-
  I thought it was time for another [update](/blog/successful-non-resolutions/)
  on my New Year's [non-resolutions](/blog/new-year-no-resolution/). I'm very
  happy to say that they're still going _really_ well. There are, of course,
  exceptions to how well it's going. 


  I've also decided to try to expand my newfound habits to include writing more.
  This is the first instance of that endeavor.
category: Life
tags:
  - tag: habits
featured: false
enabled: true
date: '2024-05-16 12:54:45'
---
I thought it was time for another [update](/blog/successful-non-resolutions/) on my New Year's [non-resolutions](/blog/new-year-no-resolution/). I'm very happy to say that they're still going _really_ well. However, no success is complete and these two are no exceptions. 

## Exercise

As I mentioned before, I had fallen down a bit on the exercise front when we went on vacation. I've missed a day or two here and there for one reason or another, but by and large I've gotten into the habit of waking up in the mornings and doing my exercises. I even managed to keep my exercise habit going through Covid! Although it wasn't _too_ hard at that point since I was only doing a couple push-ups and crunches and less than 10 lunges. 

I have another vacation coming up in a couple weeks and I'm excited to see how I do during that one. I'm fairly optimistic at this point, but I'm also acknowledging that the change in scenery and routine will be a challenge. 

## TV

February turned out to be a rough month on the TV front because of the aforementioned Covid. My wife and I both got it. It was the first time for both of us and we both just wanted to lay on the couch, stew in our misery, and be distracted by the comfort of the idiot box. So that's what we did. For about a week solid, we watched TV pretty much non-stop.

It was both a blessing and a curse.

As we started feeling better, we both talked about how much we were excited to get back to not watching TV. Even in the midst of Covid, we didn't love watching so much TV. It was just hard to do much else. Despite our excitement to stop, we both found it hard to tear ourselves away from the bad habit. Addiction is real and it's clear to me that TV can be an addiction. 

We've mostly managed to get back to not watching TV. We're not watching it on weeknights. We've watched movies on the weekends, with varying success in turning it off afterward. We always knew that we wanted to find a balance and we're in that phase now. I suspect it'll be an ongoing challenge for some time. The good thing is that we both recognize how much TV had taken over our lives, how much we appreciate having our lives back, and the struggle that we both feel finding balance. 

## Expansion

I've mentioned before that [I'd like to write a bit more](/blog/less-focus-more-writing/). A lot more, really. I read a [post on writing](https://mdalmijn.com/p/how-i-reached-32k-followers-with-a-niche-audience?utm_source=publication-search) not too long ago from [Maarten Dalmijn](https://mdalmijn.com/). He gave a few tips on how to achieve success as a writer. The one that really resonated with me was "don't worry about being original." 

I really struggle with coming up with original topics to write about. I feel like the things I have to say are mostly just repackaged versions of things that other people have already said. In most cases, lots of other people. But from now on, who cares? Maybe my repackaged version will resonate with someone. 

So this is (hopefully) the first post in a long series of unoriginal posts. 

When I started my exercise and TV non-resolutions, I mentioned [Atomic Habits](https://jamesclear.com/atomic-habits). I personally struggle to apply the incremental approach from Atomic Habits to creative endeavors. If I write half a blog post and come back the next day, I want to completely rewrite it. Or I'm just bored with the idea and want to write about something else. But there are other things in Atomic Habits that are useful.

One is to have an implementation plan. James Clear refers to these as "[implementation intentions](https://jamesclear.com/implementation-intentions)." In a nutshell, you decide to do something in given situation before the situation arises. In this case, I'm going to write on my lunch hour. Sometimes work gets in the way of me taking a lunch, but that's ok. If I follow through, I'll manage to write a couple of posts a week.

A nice thing about implementation intentions is that they dovetail well with another Atomic Habits practice: [habit stacking](https://jamesclear.com/habit-stacking). In a nutshell, we tend to follow one habit with another. My implementation intention is that I'll eat lunch and then bang out a blog post. After following through on that intention enough, it'll become a habit. I'll eat lunch and automatically sit down to write. At least, that's the hope.

Another thing that Maarten Dalmijn advised in his post is to "give yourself permission to suck." So I'll be writing posts with no filter, no editing, and no proofreading. If you're still reading, that's what you got in this post. 

I've always been told that a good piece of writing includes a conclusion. In the spirit of giving myself permission to suck, here's a conclusion:

End of post.
