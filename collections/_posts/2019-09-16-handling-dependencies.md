---
layout: post
title: Handling Dependencies
snippet: >-
  One of the hallmarks of the agile mindset is [cross-functional
  teams](https://en.wikipedia.org/wiki/Cross-functional_team). If you're working
  within the [Scrum
  framework](https://scrumguides.org/scrum-guide.html#scrum-team), this means
  that the team is comprised of "members \[who] have all the skills necessary to
  create value each Sprint." The reason for this is that dependencies slow
  everything down. There are basically three different ways to approach those
  dependencies.
category: Work
featured: true
enabled: true
featuredImage:
  fill: contain
  image: /img/uploads/dependency-map.jpg
  orientation: landscape
ogDescription: >-
  One of the hallmarks of the agile mindset is [cross-functional
  teams](https://en.wikipedia.org/wiki/Cross-functional_team). If you're working
  within the [Scrum
  framework](https://scrumguides.org/scrum-guide.html#scrum-team), this means
  that the team is comprised of "members \[who] have all the skills necessary to
  create value each Sprint." The reason for this is that dependencies slow
  everything down. There are basically three different ways to approach those
  dependencies.
ogImage: /img/uploads/dependency-map.jpg
date: '2021-01-22 05:00:00'
---
One of the hallmarks of the agile mindset is [cross-functional teams](https://en.wikipedia.org/wiki/Cross-functional_team). If you're working within the [Scrum framework](https://scrumguides.org/scrum-guide.html#scrum-team), this means that the team is comprised of "members \[who] have all the skills necessary to create value each Sprint." The reason for this is that dependencies slow everything down. 

The fact that dependencies slow things down isn't a revolutionary revelation. It's well known. Unfortunately, many organizations have problems realizing the lofty ideal of cross-functional teams. So, they're left to manage dependencies. It's such a common problem that the [SAFe](https://www.scaledagileframework.com/) framework puts a large emphasis on managing dependencies. 

There are fundamentally three different ways to approach those dependencies.

### Eliminate Dependencies

This is by far the best approach to handling dependencies. The best dependency is the one that doesn't exist. 

Imagine for a moment that you're re-doing your bathroom. You're pulling out all the fixtures, stripping it down to the studs, upgrading the wiring and plumbing, putting in new dry wall, and reinstalling all new sinks, toilets, etc. 

If you were to hire a different crew for each stage, it would take weeks. A demo crew would have to come out to remove everything. A plumber would have to come out to re-do the plumbing. An electrician for the electrical, etc. Some of those folks would have to come out twice. The plumber, for example, would have to come out while there's no drywall to put in new pipes and then come out again after the drywall was back in place to install the sink and toilet. 

The main problem with this approach is the wait time between each phase. All these folks are busy with other jobs and have to schedule your remodel between everything else. 

Now imagine that one crew can do it all. They might be able to get it done in a day or two. Things go that much faster when there are no dependencies. 

In our software development life cycle workflows, most organizations and teams have lots of ways to eliminate dependencies. Usually, these are small things. They're the things that we gloss over because it's not a big deal. Unfortunately, those small things add up quickly. Eliminating as many as you can will add up just as quickly.

### Minimize Dependencies

Minimization is the second best strategy to handling dependencies. The bathroom remodel analogy breaks down here, but a real-world software example of this is a microservice architecture. 

The monolith architecture ensures that each piece is dependent on lots of other pieces. Changing one often requires changing others, and puts the whole system at greater risk. 

Breaking a monolith down into microservices that are connected to one another through APIs greatly minimizes those dependencies. You can replace large parts of any given microservice and, as long as the API interface remains unchanged, the other microservices are unaffected. 

We can often similarly decouple teams. When those handoffs are necessary, we make them as painless as possible.

### Manage Dependencies

The last approach to handling dependencies is to manage the heck out of them. You bring those dependencies to the forefront of every conversation. Talk about them often. Create maps, diagrams, flow charts, whatever you need to help everyone involved understand them. Maybe you even designate people to track them. Anything you can do to make those dependencies everyone's focus will make things go more quickly.

To be absolutely clear, this should be the last resort. You should only engage in dependency management when all options for removal and minimization have been exhausted. Furthermore, while you're managing dependencies, you should be actively working toward minimization or removal wherever possible. This is because dependency management can easily look like this:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">AFTER - <a href="https://twitter.com/hashtag/SAFe?src=hash&amp;ref_src=twsrc%5Etfw">#SAFe</a> Program Board after <a href="https://twitter.com/ScaledAgile?ref_src=twsrc%5Etfw">@ScaledAgile</a> PI Planning w/ a couple of <a href="https://twitter.com/hashtag/dependencies?src=hash&amp;ref_src=twsrc%5Etfw">#dependencies</a> <a href="https://twitter.com/hashtag/transparency?src=hash&amp;ref_src=twsrc%5Etfw">#transparency</a> <a href="https://twitter.com/hashtag/collaboration?src=hash&amp;ref_src=twsrc%5Etfw">#collaboration</a> <a href="https://t.co/dCJ3m0Xfvk">pic.twitter.com/dCJ3m0Xfvk</a></p>&mdash; Michael Stump (@michael_p_stump) <a href="https://twitter.com/michael_p_stump/status/721007389298663424?ref_src=twsrc%5Etfw">April 15, 2016</a> 
</blockquote> 
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Looking at that, it's easy to wonder how anything ever gets accomplished.
