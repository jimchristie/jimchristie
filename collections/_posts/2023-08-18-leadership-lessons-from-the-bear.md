---
layout: post
title: Leadership Lessons from The Bear
snippet: >-
  My wife and I recently finished up the second season of [_The
  Bear_](https://www.hulu.com/series/the-bear-05eb6a8e-90ed-4947-8c0b-e6536cbddd5f)
  on Hulu. While there's a lot to appreciate in the show, especially in the
  context of leadership, there is a moment in the Season Two finale that looks
  like a huge failure on the main character's part, is also a great moment where
  the positive results of his leadership are on full display.
category: Work
featured: true
enabled: true
featuredImage:
  focalPoint:
    leftRight: center
    topBottom: bottom
  image: /img/uploads/the_bear_poster.jpg
  orientation: landscape
ogDescription: >-
  My wife and I recently finished up the second season of _The Bear_ on Hulu.
  While there's a lot to appreciate in the show, especially in the context of
  leadership, there is a moment in the Season Two finale that looks like a huge
  failure on the main character's part, is also a great moment where the
  positive results of his leadership are on full display. 
date: '2023-08-18 08:00:00'
---
My wife and I recently finished up the second season of [_The Bear_](https://www.hulu.com/series/the-bear-05eb6a8e-90ed-4947-8c0b-e6536cbddd5f) on Hulu. If you're not familiar, _The Bear_ chronicles a young fine-dining chef, Carmy (short for Carmen), who takes over his deceased brother's neighborhood sandwich shop. Throughout the show, Carmy deals with staff members who don't respect him or the work, his own grief, and all of the money problems that you might expect from a small business with tight margins.

While there's a lot to appreciate in the show, especially in the context of leadership, there is a moment in the Season Two finale that looks like a huge failure on Carmy's part, but is also a great moment where the positive results of Carmy's leadership are on full display. 

Spoilers for the Season 2 finale and the overall story arc follow. You've been warned.

<div class="container">
<div class="spoiler">
The entirety of Season Two revolves around Carmy and his staff as they work toward re-opening his brother's sandwich shop as a fine-dining establishment. The finale itself portrays the "friends and family" night where the staff offers the first service prior to opening.

Carmy warns his staff prior to letting the first guests in that the friends and family night is designed to be stressful and to push everyone to their limits. He's not wrong. There are lots of small problems. They get behind. Food gets overcooked. Orders get cold. Tempers flare. They run out of forks. 

The night's failures culminate in Carmy accidentally locking himself in the walk-in refrigerator. He had been meaning to call the "fridge guy" to get the latch repaired, but had dropped the ball. The latch breaks and he finds himself stuck in the cooler, unable to help his team, during a pivotal moment that they had all been working tirelessly toward for months.  

While this is clearly not a great moment for Carmy, the team continues to work diligently toward the overall goal of providing a great experience for their guests. Throughout all the time he's been working with the team, he has set them up for success. He has invested in their educations. They have a clear vision of the evening's goal. They don't waste time trying to get Carmy out because they all know that a successful night is more important. They're empowered and knowledgeable enough to shift responsibilities around to accommodate his absence. While Carmy failed _in this moment_ he built a resilient team that could handle his failures without missing a beat. _That's_ leadership.

Not only is this team able to handle Carmy's mistake well, they're prepared to handle any unforeseen circumstances, from Carmy or elsewhere. If something pulls Carmy away from work suddenly - a family emergency, a car accident, a sudden vacation, whatever - the team will be fine. If something goes wrong at the restaurant while Carmy's not available just because it's his day off and nobody can reach him, no big deal. The team will handle it. If the restaurant runs out of forks, it's not a big deal. 

None of that is to say Carmy is perfect. He spends his time in the fridge cursing and berating himself for his mistake. He lashes out at people who come to console him. He knocks things off of shelves. It really is a low moment. But it's a _human_ moment. We all fail. We all lose control of our emotions. A good leader works toward building a team that can withstand all of that. 

</div>
</div>
