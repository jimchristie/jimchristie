---
title: I'm Starting to Worry About This Black Box of Doom
author: Jason Pargin
stars: '3'
headline: A darkly comedic exploration of how internet communities influence our world
review: >-
  ### Summary


  This novel, written by the
  [author](https://en.wikipedia.org/wiki/Jason_Pargin) of [John Dies at the
  End](https://en.wikipedia.org/wiki/John_Dies_at_the_End), follows the tale of
  two misfits who have been tasked with delivering a large black box from Los
  Angeles to Washington DC by the Fourth of July. 


  Raised on, and scarred by, online communities, the two protagonists, Abbott
  and Ether, each have difficulty interacting with society at large. Despite
  this commonality, both have wildly different views of humanity and their place
  within it. Through their road trip, they explore the nature of what it means
  to be alive in this day and age, and how they fit into society. 


  As they traverse the country, various online communities begin to become
  obsessed with the contents of the box, usually assuming some dark purpose. The
  online speculation turns into real world action, impacting the duo's progress
  in unexpected and often hilarious fashion. 


  ### Review


  Overall, this was a fun and worthwhile read. The characters are likeable,
  interesting, and (in most cases) complex. Their motivations are believable and
  relatable. And for me, personally, good character development is often enough
  to keep me interested in a book. 


  The first half of the book contains some very pointed observations about our
  modern era, both positive and negative. The imagery those observations evoke
  is both visceral and comedic. For example, in the very first paragraph, the
  author extolls a Abbott's belief that the Los Angeles Airport is a facility
  that "had been designed to make every traveler feel like they were doing it
  wrong." If you've ever been through LAX, this probably hits home for you. As
  we get to know Abbott and Ether, as well as their antagonists and a cast of
  supporting characters, observations like this evolve into worldviews that are
  both thought-provoking and comical. 


  Unfortunately, about half or maybe two-thirds of the way through, it kind of
  loses steam. We've met the characters and the book transitions to bringing its
  various plot threads together, leading to the ultimate conclusion. 


  The problem is, that it's just not that interesting of a story. Whether or not
  the box that Abbott and Ether are transporting is benign or nefarious simply
  doesn't feel that important. At its' core, it's a silly story. A nefarious
  ending wouldn't be very impactful, and a benign ending is what we'd expect. I
  won't spoil it here and say which way the story goes because, as I said, I
  think it's worth a read.


  <div class="container">

  <div class="spoiler">

  There's also a really big coincidence that ties everything together right at
  the end. I'm not really a fan of when everything comes together by coincidence
  to begin with, and in this case, it kind of feels like the author didn't
  exactly know how to bring his plot lines together, so he threw this thing in
  that would put a big, exciting ending onto the story.

  </div>

  </div>


  I've been a little down on the book in this review. That's mostly because I
  don't want to spoil the good points and they're hard to talk about without
  giving the whole thing away. Perhaps, lowered expectations of the ending will
  make it a bit better for you.
snippet: >-
  Though it lags a bit in the second half, this was overall a fun and worthwhile
  read. It's full of funny, yet rich social commentary, as well as interesting
  and relatable characters.
enabled: true
category: Book Reviews
date: '2025-02-25 09:00:25'
featuredImage:
  image: /img/uploads/black-box-of-doom-book-cover-400x608.jpg
  orientation: portrait
---

