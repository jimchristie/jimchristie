---
title: Atomic Habits
author: James Clear
stars: '5'
headline: so freaking good
review: >-
  ### General Thoughts


  This book is _everywhere_. If you haven't already heard of it, I can pretty
  much guarantee that you'll run across in a zillion different contexts in the
  next few weeks and months. There's a good reason for that too: it's
  _excellent_.


  Atomic Habits is an easy to read manual that will help you to transform your
  life. That's not an exaggeration . A lot of books, gurus, and charlatans will
  promise that, but this one delivers. I've [talked about
  it](/blog/new-year-no-resolution/) before in this blog. And I've talked about
  the [successes that I've achieved](/blog/successful-non-resolutions/) with it
  [more than once](/blog/resolution-updates-expansion/). 


  ### Summary


  The author, James Clear, offers a clear (no pun intended) and concise formula
  for leveraging our daily habits to build the life that we desire. He argues
  that, at our core, we _are_ a collection of our habits and that if we want to
  change ourselves, we need to learn to change our habits. He offers practical
  paths to do so, leveraging the psychological drivers to our habits. 


  One of the core ideas in the book is that we don't need, and shouldn't try, to
  make big sweeping changes. Rather, make incremental changes that lead to big
  changes. He points this out with the widely quoted and paraphrased observation
  that 1% better every day leads to a 33 fold improvement over a year. 


  ![Graph showing 1% improvement for 365
  days](/img/uploads/tiny-gains-graph-700x700.jpg)


  Want to take up running? Just put your shoes on today. Maybe tomorrow you walk
  outside. Build up to walking around the house. By the end of the year, you'll
  be running daily. 


  He also offers up the advice that we should leverage our own sense of
  identity. Our sense of identity tends to drive our actions. Musicians play
  music. Health nuts eat right. Runners run. If you want to take up running,
  call yourself a runner. Tell everyone around you that you're a runner now.
  That sense of identity will help you to reframe your thinking and motivate you
  toward your goals.


  Perhaps the most useful part of the book is the Habit Loop. The Habit Loop
  identifies the four phases that our minds go through, leading us to engage in,
  and reinforce behaviors.


  1. Cue. The thing that makes us think of the behavior.

  2. Craving. The neurological response that, essentially, equates to desire for
  the behavior. 

  3. Response. This is the behavior, or habit. 

  4. Reward. What we get out of the behavior. 


  ![Illustration of the Habit Loop](/img/uploads/habit-loop.png)


  The bulk of the book is devoted to hacking these four phases to either create
  new habits, or break old habits. He defines Four Laws of Behavior Change, one
  for each of the part of The Habit Loop. The laws are easy to remember and
  apply in daily life. Each one individually helps to create a new habit or
  break an old one. Employing all four of them is an incredibly powerful recipe
  for behavior change. 


  It also helps you to reframe the idea of failure. James Clear offers up the
  analogy that each time you engage in your new habit, you're casting a vote for
  the future version of yourself that you're trying to build. One vote isn't
  enough to change an election. It takes lots of them. Don't beat yourself up.
  Just cast more votes for who you want to be. Personally, I found this to be a
  powerful metaphor. I think about it every time I fail. It helps me to keep my
  spirits up and to motivate me to do better next time. 


  ### Conclusion


  This book is truly life changing. It's honestly only one of two books that
  I've found to be so impactful, and in such a directly applicable way. The
  other was [How to Read a Book](/blog/how-to-read-a-book/). 


  Unlike How to Read a Book, Atomic Habits is incredibly approachable and easy
  to read. My wife and I read it together and her main complaint was that it's a
  bit repetitive. I, however, found the repetition to be a good thing in that it
  reinforces and reframes ideas. 


  It's been a couple of years now since I read this book and every time that I
  find myself engaging in a bad habit or wanting to build a new one, I think
  about the habit loop and look for ways to apply one of the Four Laws. Often
  times, applying a single law is enough. And when it's not, there are three
  more to draw from and I can cast another vote next time. 
snippet: >-
  This book is _everywhere_. If you haven't already heard of it, I can pretty
  much guarantee that you'll run across in a zillion different contexts in the
  next few weeks and months. There's a good reason for that too: it's
  _excellent_.


  Atomic Habits is an easy to read manual that will help you to transform your
  life. That's not an exaggeration . A lot of books, gurus, and charlatans will
  promise that, but this one delivers. 
enabled: true
category: Book Reviews
date: '2024-05-24 11:07:00'
featuredImage:
  image: /img/uploads/atomic-habits-cover.jpg
  orientation: portrait
---

