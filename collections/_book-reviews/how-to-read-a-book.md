---
title: How to Read a Book
author: Mortimer J. Adler & Charles Van Doren
stars: '5'
headline: A _must-read_ for anyone who wants to get more out of books
review: >-
  ### General Thoughts


  This classical work has been on my reading list for almost 20 years. One of my
  college professors (I wish I could remember his name) recommended it on the
  first day of class. He asserted that even though all of us knew how to read,
  most of us did not know how to read \_well\_. He told us that this book would
  teach us how to do that. 


  I didn't put off reading it because I thought he was wrong. He was a
  remarkable man, clearly very intelligent, well-read himself, an experienced
  professor, and a good judge of his students' abilities. I pretty much assumed
  that he was right. The reason I put it off was because I was just so _busy_. I
  had other classes. I had extracurriculars. I wanted to set aside time to have
  a little fun. It _was_ college after all. 


  I should have listened to him a long time ago. 

  _How to Read a Book_ is simply remarkable. Had I listened to my professor at
  the time, this one book would have saved me a ton of time reading others and
  improved my grades as well. Had I followed his advice, I likely would have
  read more and understood better in the years after college as well.


  Adler and Van Doren have given more thought to books, reading, readers,
  authors, and the relationships amongst them than I have given
  to...well...likely anything at all. Their observations about how to read, how
  to interpret and criticize an author, and how to read across different works
  are simply profound. What's more, they teach you how to determine which books
  are worth your time and which aren't _before_ you've taken the time to read
  it. (You might think I'm talking about skimming here, and you wouldn't be
  exactly wrong, but you wouldn't exactly be right either.)


  ### Summary


  Adler and Van Doren break their book into four parts. The first three parts
  are to three levels of  reading a single book. The fourth and final part
  focuses on how to read across several works on a single topic. At each stage
  the authors provide a number of guidelines and rules for reading a book, along
  with reasons and arguments that reinforce the need for those rules. 


  Part one discusses the first two levels of reading, as defined by the authors:
  _Elementary Reading_ and _Inspectional Reading_. 


  Elementary Reading is the level that most people reach. It's the simple
  process of stringing letters into words, words into sentences, sentences into
  paragraphs, and paragraphs into larger cohesive collections of thought. The
  few people who go on to learn more reading skills beyond that typically focus
  on reading _faster_ not _better_.


  The goal of Inspectional Reading is to get a big picture of the work, how it
  is structured and what its main arguments are. It is akin to skimming and does
  involve some traditional skimming, but is far more structured and deliberate.
  When most people skim, they simply skip through and try to pick out important
  parts of the text. Inspectional reading starts out focusing on the broadest
  possible aspects of the work; the title page, publisher's blurb, and table of
  contents; then moves into progressively more specific parts of the work such
  as section headers and opening and closing paragraphs. Finally, the
  Inspectional Reader will dip in here and there to try to grasp a bit more of
  the author's main message and arguments. 


  Part two of the book focuses on the third level of reading: _Analytical
  Reading_. This stage of reading is dedicated to the evaluation and reading of
  various works. It is primarily focused on how to read nonfiction or, as they
  refer to it, _expository_ works. The goal of this section is to help the
  reader see his or her relationship and obligations to the author (Yes! Readers
  have relationships obligations to authors! Who knew?), evaluate the arguments
  presented by the author, and determine what to do with the new information
  presented. 


  Part three gets into specific rules for specific types of work. Not only are
  there chapters for a variety of types of expository works - philosophy,
  various sciences, mathematics, and more - but the authors also dedicate two
  chapters to imaginative works such as poetry, literature, and plays. 


  Finally, the fourth part is dedicated to reading several works across a
  subject, which the authors dub the fourth level of reading and label it
  _Syntopical Reading_. The authors give advice on how to define the area of
  concern within the reader's subject, how to choose books, and give revised
  versions of their rules for the various levels of reading. 


  ### Conclusion


  I truly can not overstate how much I enjoyed this book. It has changed the way
  I've approached every book I've read since starting it. There are a few books
  that I opted not to move past the inspectional level because I was able to
  tell that they were poorly constructed arguments, weren't up to the level I
  needed on that particular topic, or both. Those that I have chosen to move
  past the inspectional stage with, I have understood better and read faster
  (even though the book only devotes about two sentences to faster reading
  techniques). 


  The one caution I would give to prospective readers of this book is that it is
  a very challenging read. As I mentioned, the authors have given a great deal
  of thought to the act of reading and have structured very detailed arguments
  to support their reasoning for framing the act of reading in the way that they
  have. They are, unsurprisingly, well-read themselves and often use books
  unfamiliar to  most of us as examples (Darwin's _Origin of Species_ and the
  various works of Aristotle come up regularly).


  Even in spite of that one caution, the book is absolutely eye-opening. It has
  already become a regularly referenced work on my shelf, and will continue to
  be for quite some time. I _thought_ I knew how to read before reading this
  book. Now I _know_ I do.
snippet: >-
  This classical work has been on my reading list for almost 20 years. One of my
  college professors (I wish I could remember his name) recommended it on the
  first day of class. He asserted that even though all of us knew how to read,
  most of us did not know how to read _well_. He told us that this book would
  teach us how to do that. 


  I should have listened to him a long time ago.
enabled: true
category: Book Reviews
date: '2024-05-17 12:47:00'
featuredImage:
  image: /img/uploads/how-to-read-a-book.jpg
  orientation: portrait
---

