---
title: Empowered
author: Marty Kagan
stars: '4'
headline: Pretty dang good
affiliateLink: 'https://affiliate.link'
review: Pretty good. Not a lot that was new. Still a nice refresher.
date: '2022-07-07 12:00:00'
enabled: false
category: Book Reviews
---

