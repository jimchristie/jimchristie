---
title: Easy guideline for recognizing misinformation
text: >-
  If you hear something that's so terrible it makes you think "Oh my god!" and
  it's unique (i.e., not a terrible train crash or something similar), you
  should probably have a healthy amount of skepticism.
date: '2024-09-13 10:10:05'
category: Blurbs
---

