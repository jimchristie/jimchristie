---
site-name: Bicycles Stack Exchange
username: jimchristie
profile: 'https://bicycles.stackexchange.com/users/4239/'
icon-code: <span class="fab fa-stack-exchange"></span>
license: Creative Commons Attribution-ShareAlike 4.0 International
---

