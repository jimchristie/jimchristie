---
title: Agile Coaching Retreat Live Online
mostRecentYear:
  endDate: '2020-09-18'
  startDate: '2020-09-14'
link: 'https://www.scrumalliance.org/events/coaching'
description: >-
  The Scrum Alliance Agile Coaching Retreat is for anyone interested in agile
  coaching or who wants to improve their agile coaching skills/network.
  Additionally, more experienced coaches will hone their talents alongside the
  best and brightest agile coaching minds in the field.
logo: /img/uploads/agile-coaching-retreat-live-online.jpg
previousYears: []
---

