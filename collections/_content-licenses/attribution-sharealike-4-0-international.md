---
name: Creative Commons Attribution-ShareAlike 4.0 International
abbreviation: CC BY-SA 4.0
link: 'https://creativecommons.org/licenses/by-sa/4.0/'
icon-codes:
  - icon-code: <i class="fa-brands fa-creative-commons"></i>
  - icon-code: <i class="fa-brands fa-creative-commons-by"></i>
  - icon-code: <i class="fa-brands fa-creative-commons-sa"></i>
---

