---
title: Agile Coach and Trainer
company: Owlsy
snippet: >-
  Owlsy is a small group of dedicated coaches and trainers who specialize in
  helping businesses find ways to become more customer-focused and agile. 


  As an Agile Coach and Trainer, I help clients to see and realize the benefits
  of working in collaborative and customer-focused ways.
startDate: 2021-06-01
endDate: 9999-12-31
link: 'https://owlsy.io'
logo: /img/uploads/owlsy-image-only-logo.png
featured: true
enabled: true
---
Owlsy is a small group of dedicated coaches and trainers who specialize in helping businesses find ways to become more customer-focused and agile. 

As an Agile Coach and Trainer, I help clients to see and realize the benefits of working in collaborative and customer-focused ways.
