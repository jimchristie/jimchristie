---
title: Lead Agile Trainer
company: Hanesbrands Inc.
snippet: >-
  As part of its Full Potential Plan, Hanes Brands Inc. was developing more
  agile ways of working. As an Agile Trainer in this initiative, I developed
  training content for and delivered training content to Scrum Teams, individual
  Scrum Team members, stakeholders, and members of management.
startDate: '2021-09-27'
endDate: '2022-10-14'
link: 'https://www.hanes.com/corporate'
skills: []
logo: /img/uploads/hanesbrands-logo.png
featured: false
enabled: true
---
As part of its Full Potential Plan, Hanes Brands Inc. was developing more agile ways of working. 



As a Lead Agile Trainer in this initiative, I developed training content for and delivered training content to Scrum Teams, individual Scrum Team members, stakeholders, and members of management. Working in partnership with Agile Coaches and the Organizational Readiness department, I prioritized what was the next most valuable training course to develop, and executed that development.
