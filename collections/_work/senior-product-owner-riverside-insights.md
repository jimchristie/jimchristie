---
title: Senior Product Owner
company: Riverside Insights
snippet: >-
  Riverside Insights helps teachers evaluate students' needs and skills through
  a variety of assessment programs. As a Senior Product Owner, I helped to
  deliver a first-in-class testing and proctoring platform.
startDate: '2022-10-24'
endDate: 9999-12-31T00:00:00.000Z
logo: /img/uploads/riverside-insights-logo.png
featured: true
enabled: true
---
Riverside Insights helps teachers evaluate students' needs and skills through a variety of assessment programs. In an effort to modernize their technology practices, Riverside sought to deliver a first-in-class testing and proctoring platform.

As a Senior Product Owner at Riverside Insights, I helped to evaluate the needs of the platform. Using data from the existing platform, input from internal stakeholders, and customer feedback, I worked with the Product Management and Engineering teams to prioritize features for delivery.

Accomplishments:

* Delivered a cutting-edge test administration platform for K-12 students
* Introduced a number of design improvements based on User Experience best practices
