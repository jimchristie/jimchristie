---
title: Agile Facilitator
company: Pearson Virtual Schools
snippet: >-
  Pearson Virtual Schools provides digital solutions to students and teachers
  who are making a shift to virtualized learning. Virtual Schools are at the
  forefront of Pearson's transition to digital learning and learning materials.
startDate: 2020-01-13
endDate: 2021-05-19
link: >-
  https://www.pearson.com/us/prek-12/products-services-teaching/online-blended-learning-solutions.html
logo: /img/uploads/pearson-logo-simple.png
featured: true
enabled: true
---
Pearson Virtual Schools provides digital solutions to students and teachers who are making a shift to virtualized learning. Virtual Schools are at the forefront of Pearson's transition to digital learning and learning materials.

As an Agile Facilitator, I coach and train teams and individuals to deliver working software in a manner that allows the business to pivot to changing market needs.

Accomplishments:

* Designed and implemented a remote Scrum training program
* Facilitate dependency coordination between teams
* Worked with RTEs to redesign PI Planning
