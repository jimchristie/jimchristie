---
title: Designing and Building AI Products and Services
startDate: '2024-10-16'
endDate: '2024-12-16'
link: >-
  https://certificates.emeritus.org/1c907a9c-894a-4909-a6fe-d22eff97584b#acc.vGje9CJR
description: >-
  This course is ideal for you if you are a technical product leader, technology
  professional, technology consultant, or entrepreneur who wants to enhance your
  understanding of AI technology fundamentals and tools, and explore various
  design processes involved in AI-based products. Knowledge of calculus, linear
  algebra, statistics, and probabilities is beneficial, along with basic Python
  experience.
provider:
  link: >-
    https://mit-xpro-online-education.emeritus.org/designing-building-ai-products-services?utm_source=Google&utm_network=g&utm_medium=c&utm_term=mit%20artificial%20intelligence%20program&utm_location=9023535&utm_campaign_id=17059210703&utm_adset_id=137398429433&utm_ad_id=616782048367&gad_source=1&gclid=Cj0KCQjwgrO4BhC2ARIsAKQ7zUnnnVQSRkhcGVWtd063NaYOgKfTthwyi-lN5EUXYsR7AmBkcRTapoAaAiDrEALw_wcB
  name: Massachusetts Institute of Technology
trainers: []
---

