---
title: Training from the Back of the Room - Virtual Edition
startDate: '2022-11-03'
endDate: '2022-11-17'
link: 'https://www.bowperson.com/public-training-events/'
description: >-
  In this practical and interactive 5-module virtual class, you’ll explore
  current “brain science” as it relates to human learning in virtual
  environments. You’ll discover 6 specific brain-science principles and a 4-step
  instructional design model that you can use in your own virtual teaching and
  training. And you’ll leave with dozens of new ideas, activities, resources,
  and a new “Gold Standard” for virtual instruction and learning.
trainers:
  - trainer:
      link: 'https://angelaagresto.com/'
      name: Angela Agresto
  - trainer:
      link: 'https://www.linkedin.com/in/kbruns/'
      name: Karen Bruns
provider:
  name: Monarch Coaching & Training
  link: https://monarchcoachingllc.com/
---

