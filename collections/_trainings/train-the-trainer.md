---
title: Training from the Back of the Room
startDate: '2017-08-17'
endDate: '2017-11-13'
link: 'https://bowperson.com/training-from-the-back-of-the-room/'
description: >-
  In the TBR Practitioner Class, you’ll explore how the human brain really learns, which is very different from traditional assumptions about learning. 
  And you’ll be introduced to the most current “cognitive neuroscience” – the brain science behind all effective instructional design and human learning. 
  Agile42's course goes beyond the basic course and includes additional time for real world practice and evaluation of the classroom materials. 
provider:
  link: 'https://www.agile42.com/en/'
  name: Agile 42
trainers:
  - trainer:
      link: 'https://www.linkedin.com/in/lklosebc/'
      name: Lukas Klose
  - trainer:
      link: 'https://www.linkedin.com/in/dhavalpanchal/'
      name: Dhaval Panchal
---

