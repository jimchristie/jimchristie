---
title: bikablo Basics Day 2
startDate: '2024-04-16'
endDate: '2024-04-18'
link: 'https://bikablo.com/basics-trainings/'
description: >-
  Whether you are a beginner “with two left hands” or a self-taught person – the
  bikablo visualization technique is your springboard into the wonderful world
  of visual thinking. ​
trainers:
  - trainer:
      link: 'https://jillgreenbaum.com/'
      name: Jill Greenbaum
---

