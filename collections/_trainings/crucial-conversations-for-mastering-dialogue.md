---
title: Crucial Conversations for Mastering Dialogue
startDate: '2022-03-07'
endDate: '2022-03-11'
link: 'https://cruciallearning.com/crucial-conversations-for-dialogue/'
description: >-
  Backed by 30 years of social science, Crucial Conversations skills represent
  the standard in effective communication and the marker of high performance
  individuals and organizations.
provider:
  link: 'https://cruciallearning.com/'
  name: Crucial Learning
trainers:
  - trainer:
      link: 'https://www.linkedin.com/in/bethwolfson/'
      name: Beth Wolfson
---

