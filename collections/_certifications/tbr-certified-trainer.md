---
title: TBR Certified Trainer
authority: bowperson.com
logo: /img/uploads/tbr-ct-logo-hi-res.png
certificationDate: '2022-06-11'
expirationDate: ''
link: /img/uploads/TBR-CT-Certificate-James-Christie.pdf
featured: true
---

