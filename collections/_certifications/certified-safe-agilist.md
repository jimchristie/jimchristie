---
title: Certified SAFe Agilist
authority: Scaled Agile
logo: /img/uploads/cert_mark_sa5_small_150px.png
certificationDate: '2020-08-04'
expirationDate: '2023-08-05'
link: >-
  https://www.youracclaim.com/badges/c054742e-f4f2-43e5-9aca-48d30e5633c6/public_url
featured: false
---

