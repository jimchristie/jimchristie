---
title: Pragmatic Certified Product Manager
authority: Pragmatic Institute
logo: /img/uploads/pragmatic-pm-badge.png
certificationDate: '2023-08-17'
expirationDate: ''
link: 'https://www.credly.com/badges/0d24ec44-4e10-4aef-b7c5-2f594e2576a2/public_url'
featured: true
---

