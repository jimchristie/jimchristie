---
title: TBR-VE Certified Trainer
authority: bowperson.com
logo: /img/uploads/tbr-ve-ct-logo-hi-res.png
certificationDate: '2023-12-11'
expirationDate: ''
link: /img/uploads/TBR-VE-CT-Certificate-Jim-Christie.pdf
featured: false
---

