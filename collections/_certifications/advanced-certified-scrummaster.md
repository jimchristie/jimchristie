---
title: Advanced Certified ScrumMaster
authority: Scrum Alliance
logo: /img/uploads/a-csm.png
certificationDate: '2018-01-26'
expirationDate: '2025-01-19'
link: 'https://www.scrumalliance.org/community/profile/jchristie2'
featured: false
---

