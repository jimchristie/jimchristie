---
title: Certified Scrum Product Owner
authority: Scrum Alliance
logo: /img/uploads/CSPO_Logo.png
certificationDate: '2016-06-08'
expirationDate: '2025-01-19'
link: 'https://www.scrumalliance.org/community/profile/jchristie2'
featured: true
---

