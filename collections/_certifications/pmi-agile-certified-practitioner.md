---
title: PMI Agile Certified Practitioner
authority: Project Management Institute
logo: /img/uploads/pmi-acp-redesign-600px.png
certificationDate: '2017-04-06'
expirationDate: '2026-04-05'
link: >-
  https://www.youracclaim.com/badges/97f17f67-7481-4691-9b30-f4f08a004bb4/public_url
featured: true
---

