---
title: Certified ScrumMaster
authority: Scrum Alliance
logo: /img/uploads/CSM_Logo.png
certificationDate: '2016-06-02'
expirationDate: '2025-01-19'
link: 'https://www.scrumalliance.org/community/profile/jchristie2'
featured: false
---

