---
title: Certified Agile Leadership 1
authority: Scrum Alliance
logo: /img/uploads/cal-1-badge.png
certificationDate: '2018-11-05'
expirationDate: '2025-01-19'
link: 'https://bcert.me/swifxtfgx'
featured: true
---

