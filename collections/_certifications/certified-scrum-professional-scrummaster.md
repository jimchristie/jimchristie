---
title: Certified Scrum Professional - ScrumMaster
authority: Scrum Alliance
logo: /img/uploads/csp-sm_logo.png
certificationDate: '2019-01-13'
expirationDate: '2025-01-19'
link: 'https://www.scrumalliance.org/community/profile/jchristie2'
featured: true
---

