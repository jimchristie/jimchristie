let filterButtons = document.querySelectorAll(".category-filter");
let posts = document.querySelectorAll(".post");
let postList = document.getElementById('collection-index');

filterButtons.forEach(button => {
	button.addEventListener("click", function handleClick(event) {
		toggleButtons(filterButtons, button);
        extraLazyLoadImages();
	});
});

// this allows time for the css transitions to run
function extraLazyLoadImages() {
    setTimeout(() => { 
        lazyLoadImages();
    }, 250);
}

function setURLParams(params) {
    let paramString = '?';
    for (const [key, value] of Object.entries(params)) {
        paramString = paramString.concat(key.toString(), '=', value.toString(), '&');
    }
    
    paramString = paramString.slice(0, paramString.length - 1);
    
    window.history.pushState(null, null, paramString);
}

function filterPosts(filter) {
	posts.forEach(post => {
		let tags = post.dataset.hasOwnProperty('tags') ? post.dataset.tags.split(', ') : [];
		
		if (postShouldBeHidden(filter, post.dataset.category, tags)) {
			hidePost(post);
//		} else if (post.dataset.filtered) {
        } else {
			unhidePost(post);
		}
		
	});
}

function postShouldBeHidden(filter, category, tags) {
	return category !== filter && !filterIsInTags(filter, tags);
}

function filterIsInTags(filter, tags) {
	if (typeof tags === 'undefined' || tags.length === 0)
		return false;
	
	for (let i = 0; i < tags.length; i++) {
		if (filter === tags[i])
			return true;
	}
}

function hidePost(post) {
	post.classList.add('hidden');
	post.classList.add('hidden-by-filter');
	post.setAttribute('aria-hidden', 'true');
	post.dataset.filtered = true;
	extraLazyLoadImages();
}

function unhidePost(post) {
	post.classList.remove('hidden');
	post.classList.remove('hidden-by-filter');
	post.removeAttribute('aria-hidden');
	delete post.dataset.filtered;
}


function clearQueryParams() {
    let url = window.location.href;
    url = url.slice(0, url.indexOf('?'));
    
    window.history.pushState(null, null, url);
}

function unhideAllPosts() {
	posts.forEach(post => {
		unhidePost(post);
	});
}

function toggleButtons(buttons, button) {
	if (!button.classList.contains('active')){
		setButtonAsActive(button);
		filterPosts(button.dataset.category);
        setURLParams( {'filter': button.dataset.category} );
		for (let i = 0; i < buttons.length; i++) {
			if( buttons[i] !== button){
				setButtonAsInactive(buttons[i]);
            }
		}
	} else {
		unhideAllPosts();
        unsetAllActiveInactiveButtonSettings();
        clearQueryParams();
	}
}

function setButtonAsActive(button) {
		button.classList.add('active');
		button.classList.remove('inactive');
}

function setButtonAsInactive(button) {
	button.classList.add('inactive');
	button.classList.remove('active');
}

function setAllButtonsAsInactive() {
	filterButtons.forEach(button => {
		setButtonAsInactive(button);
	});
}

function unsetActiveInactiveButtonSetting(button) {
    button.classList.remove('inactive');
    button.classList.remove('active');
}

function unsetAllActiveInactiveButtonSettings() {
    filterButtons.forEach(button => {
           unsetActiveInactiveButtonSetting(button);
    });
}

function filterParamExists() {
    var field = 'filter';
    var url = window.location.href;
    if(url.indexOf('?' + field + '=') != -1)
        return true;
    else if(url.indexOf('&' + field + '=') != -1)
        return true;
    return false
}

// TODO: something about this function is failing on the Q&A. It's returning the 'q-&-a' value as 'q-'
// Fix before going live with Q&A post types
function getQueryParams() {
    let params = window.location.search.slice(1, window.location.search.length);
    params = new URLSearchParams(window.location.search.slice(1, window.location.search.length));
    
    let paramJSON;
    
    if (filterParamExists()) {
        paramJSON = {};
        params.forEach((value, key) => {
           paramJSON[key] = value;
        });
    }
    
    return paramJSON;
}

function filterPostsFromQueryParams() {
    let params = getQueryParams();
    
    if (typeof params != 'undefined' && typeof params.filter != undefined) {
        
        let activeButton;
        
        filterButtons.forEach(button => {
            if (button.dataset.category === params.filter) {
                activeButton = button;
            }
        });
        
        filterPosts(params.filter);
        toggleButtons(filterButtons, activeButton);
        
        // this won't override the filter above
//        showAllPosts();
    }
    
    // this page starts with the whole index hidden to prevent jumpiness
    // removing this class after the filtering helps (still more I could do)
    postList.classList.remove("display-none");
}

// init events

window.addEventListener("load", (event) => {
    filterPostsFromQueryParams();
});

window.addEventListener('popstate', function (event) {
    filterPostsFromQueryParams();
});
