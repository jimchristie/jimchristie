
/*
 ********************** spoilers *************************************************
 */

function setSpoilerHeight(spoiler) {
  spoiler.parentNode.classList.add('contains-spoilers');
  
}

function createSpoilerOverlay() {
  let overlay = document.createElement("div");
  overlay.classList.add('overlay-box');
  
  for ( let i = 0; i < 8; i++ ) {
    let overlayLine = document.createElement("div");
    overlayLine.classList.add('overlay-line');
    overlay.prepend(overlayLine);
  }
  return overlay;
}

function createRevealElements(spoilerContainer) {
  let revealContainer = document.createElement("div");
  revealContainer.classList.add('reveal-container');
  revealContainer.append(createSpoilerWarning());
  revealContainer.append(createRevealButton());
  spoilerContainer.prepend(revealContainer);
  
}

function createSpoilerWarning() {
  let warning = document.createElement("p");
  warning.classList.add('spoiler-warning');
  warning.innerText = "Contains Spoilers";
  return warning;
}

function revealSpoiler() {
  this.parentNode.parentNode.classList.add('revealed-spoiler');
}

function createRevealButton(){
  let button = document.createElement("p");
  button.innerText = "Show Spoilers";
  button.classList.add('button', 'white', 'reveal-button');
  button.addEventListener('click', revealSpoiler);
  return button;
}

function hideSpoilers() {
    let spoilers = document.getElementsByClassName('spoiler');

    for ( let i = 0; i < spoilers.length; i++ ) {
      let spoiler = spoilers[i];
      setSpoilerHeight(spoiler);
      
      
        let spoilerOverlay = createSpoilerOverlay();
        spoiler.parentNode.prepend(spoilerOverlay);
      
      createRevealElements(spoiler.parentNode);
    }
}
 
 
 hideSpoilers();